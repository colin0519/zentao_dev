"""zentao_second_dev URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    # 看板页
    path('dashboard/', include('dashboard.urls')),
    # 项目清单
    path('project/', include('projectData.urls')),
    # 子系统清单
    path('subsystem/', include('projectData.urls')),
    # Bug统计与详情
    path('Bug/', include('BugStatics.urls')),
    # 用例执行与统计
    path('case/', include('executeStatistics.urls')),
    # 导出
    path('download/', include('download.urls')),
    # 配置文件
    path('config/', include('configData.urls')),
    # 缺陷
    path('defect/', include('defect.urls')),
    # 测试用例
    path('testCase/', include('testCase.urls')),
    # 个人数据
    path('personData/', include('personData.urls')),
    # 迭代任务
    path('task/', include('task.urls')),
]
