from django.contrib import admin
from django.urls import path
from download import views

urlpatterns = [
    path('caseStatistics', views.downloadCaseStatistics),
    path('projectIterationExtension', views.downloadProjectIterationExtension),
    path('subsystemIterationExtension', views.downloadSubsystemIterationExtension),
    path('versionIterationExtension', views.downloadVersionIterationExtension),
    path('BugStatistics', views.downloadBugStatistics),
]
