# Create your views here.
import json

from django.http import HttpResponse, HttpResponseBadRequest
import pickle

from common.constant import department_person_file, pkl_key_dict, person_role_file, authority_file
from common.sql_constant_enum import account_role_types, authority_type_map
from common.sql_instance_config import *
from utils.cursorHandler import cursor
from utils.normal_simple_function import serialize_json_dumps, read_pkl_file, write_pkl_file, serialize_json_date_type
from datetime import datetime


async def departmentPersonShow(request):
    if request.method == 'GET':
        department_id = int(request.GET.get('department_id', -1)) if request.GET.get('department_id', -1) else -1
        person_id = int(request.GET.get('person_id', -1)) if request.GET.get('person_id', -1) else -1
        person_role_id = int(request.GET.get('person_role_id', -1)) if request.GET.get('person_role_id', -1) else -1
        # tester_name_list = cursor.search_alone(department_person_info_list_sql)
        # 获取人员角色 --global--
        person_role_info_list = cursor.search_alone(person_role_info_list_sql)
        person_role_dict = {each['id']: each['name'] for each in person_role_info_list}
        # 获取部门信息 --global--
        department_info_list = cursor.search_alone(department_info_sql)
        department_name_dict = {each['dept_id']: each['dept_name'] for each in department_info_list}

        person_roles = read_pkl_file(person_role_file, pkl_key_dict[person_role_file])
        # 对结果做过滤
        person_return_info = {}
        json_returns = {}
        has_checked_flag = False
        # 有查询条件
        if person_id > 0 or person_role_id > 0 or department_id > 0:
            for each_tester_id, each_tester in person_roles.items():
                # 部门与角色均有
                if person_role_id > 0 and department_id > 0 and each_tester['person_role_id'] == person_role_id and department_id == each_tester['department_id']:
                    person_return_info.update({each_tester_id: person_roles.get(each_tester_id, None)})
                # 角色
                elif person_role_id > 0 and each_tester['person_role_id'] == person_role_id:
                    if department_id > 0 and department_id != each_tester['department_id']:
                        has_checked_flag = True
                    else:
                        person_return_info.update({each_tester_id: person_roles.get(each_tester_id, None)})
                # 部门
                elif department_id > 0 and department_id == each_tester['department_id']:
                    if person_role_id > 0 and each_tester['person_role_id'] != person_role_id:
                        has_checked_flag = True
                    else:
                        person_return_info.update({each_tester_id: person_roles.get(each_tester_id, None)})
            else:
                # 如果有人员，则需要排除其他，得到3者(2者)交集
                if person_id > 0 and person_id in person_return_info:
                    person_return_info = {person_id: person_return_info[person_id]}
                # 其他2条件无，只有人员
                elif person_id > 0 and person_role_id < 0 and department_id < 0:
                    person_return_info = {person_id: person_roles.get(person_id, None)}
                elif person_id < 0:
                    pass
                # 其他2条件不匹配(错误)，只有人员
                else:
                    person_return_info = {}
            if person_return_info:
                json_returns = {
                    'data': {
                        "person_infos": [
                            {
                                "id": each_tester.get("person_id"),
                                "dpt_name": department_name_dict.get(each_tester.get("department_id"), None),
                                "dpt_id": each_tester.get("department_id"),
                                "tester_name": each_tester.get("person_name"),
                                "tester_role": person_role_dict.get(each_tester.get("person_role_id"), None),
                                "tester_role_id": each_tester.get('person_role_id'),
                                "operator": each_tester.get("operator"),
                                "create_time": serialize_json_date_type(each_tester.get("create_time")),
                                "desc_msg": each_tester.get("desc_msg"),
                            } for each_tester in person_return_info.values() if each_tester
                        ]
                    }
                }
            else:
                json_returns = {
                    'data': {
                        "person_infos": []
                    }
                }

        # 无查询条件
        else:
            # print('person_roles ==>', person_roles)
            if person_roles:
                code_type = "success"
                json_returns = {
                    'data': {
                        "person_infos": [
                            {
                                "id": each_tester.get("person_id"),
                                "dpt_name": department_name_dict.get(each_tester.get("department_id"), None),
                                "dpt_id": each_tester.get("department_id"),
                                "tester_name": each_tester.get("person_name"),
                                "tester_role": person_role_dict.get(each_tester.get("person_role_id"), None),
                                "tester_role_id": each_tester.get('person_role_id'),
                                "operator": each_tester.get("operator"),
                                "create_time": serialize_json_date_type(each_tester.get("create_time")),
                                "desc_msg": each_tester.get("desc_msg"),
                            } for each_tester in person_roles.values()
                        ]
                    }
                }
            else:
                json_returns = {}

        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def departmentPersonAdd(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 部门id
        department_id = params.get('department_id', -1)
        # 人员名称
        person_name = params.get("person_name", "")
        # 人员id
        person_id = params.get("person_id", -1)
        # 人员角色id
        person_role_id = params.get("person_role_id", -1)
        # 备注信息
        desc_msg = params.get('desc_msg', "")

        person_roles = read_pkl_file(person_role_file, pkl_key_dict[person_role_file])
        # print('person_roles ==>', person_roles)

        if person_roles:
            if person_id in person_roles:
                code_type = "duplicated"
            else:
                if person_id:
                    code_type = "success"
                    person_roles.update(
                        {
                            person_id: {
                                "department_id": department_id,
                                "person_name": person_name,
                                "person_id": person_id,
                                "person_role_id": person_role_id,
                                "desc_msg": desc_msg,
                                "operator": "admin",
                                "create_time": serialize_json_date_type(datetime.now())
                            }
                        }
                    )
                    write_pkl_file(person_role_file, pkl_content=person_roles, key_word=pkl_key_dict[person_role_file])

                else:
                    code_type = "value_invalid"
        else:
            code_type = "success"
            person_roles = {}
            person_roles[pkl_key_dict[person_role_file]] = {
                    person_id: {
                        "department_id": department_id,
                        "person_name": person_name,
                        "person_id": person_id,
                        "person_role_id": person_role_id,
                        "desc_msg": desc_msg,
                        "operator": "admin",
                        "create_time": serialize_json_date_type(datetime.now())
                    }
                }
            write_pkl_file(person_role_file, pkl_content=person_roles, key_word='')
        json_returns = {
            'data': {
                "person_id": person_id,
                "person_name": person_name,
                "operator": "admin",
                "create_time": serialize_json_date_type(datetime.now())
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns, code_type=code_type), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def departmentPersonSet(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 部门id
        department_id = params.get('department_id', -1)
        # 人员名称
        person_name = params.get("person_name", "")
        # 人员id
        person_id = params.get("person_id", -1)
        # 人员角色id
        person_role_id = params.get("person_role_id", -1)
        # 备注信息
        desc_msg = params.get('desc_msg', "")

        if department_id > 0 or person_name or person_id > 0 or person_role_id > 0 or desc_msg:
            person_roles = read_pkl_file(person_role_file, pkl_key_dict[person_role_file])
            # print('person_roles ==>', person_roles)
            if person_id in person_roles:
                if department_id > 0:
                    person_roles[person_id]['department_id'] = department_id
                if person_role_id > 0:
                    person_roles[person_id]['person_role_id'] = person_role_id
                if desc_msg:
                    person_roles[person_id]['desc_msg'] = desc_msg
                code_type = "success"
                write_pkl_file(person_role_file, person_roles, key_word=pkl_key_dict[person_role_file])
            else:
                code_type = "value_not_exists"
        else:
            code_type = "success"
        json_returns = {
                'data': {
                    "person_id": person_id,
                    "person_name": person_roles[person_id]["person_name"] if person_roles and person_roles.get(person_id) else None,
                    "operator": "admin",
                    "create_time": serialize_json_date_type(datetime.now())
                }
            }
        response = HttpResponse(serialize_json_dumps(json_returns, code_type=code_type),
                                    content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')

async def departmentPersonSet_old(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 新添加列表
        add_list = params.get('add_list', [])
        # 删除列表
        exclude_list = params.get('exclude_list', [])

        with open(department_person_file, 'rb') as f:
            tester_name_list = pickle.load(f)
        tester_list = tester_name_list['person_infos']
        # 查询新增加的列表
        if add_list:
            add_sql = construct_department_person_update_sql(search_list=add_list)
            print('add_sql', add_sql)
            add_info_list = cursor.search_alone(add_sql)
            tester_list.extend(list(add_info_list))
            print(tester_list)
        if exclude_list:
            exclude_sql = construct_department_person_update_sql(search_list=exclude_list)
            print('exclude_sql', exclude_sql)
            exclude_info_list = cursor.search_alone(exclude_sql)
            if exclude_info_list:
                tester_list = [tester_info for tester_info in tester_list if tester_info not in exclude_info_list]
        # 将结果存入文档中
        with open(department_person_file, 'wb') as file_dpt_pers:
            pickle.dump(
                {
                    "person_infos": tester_list
                }, file_dpt_pers
            )
        json_returns = {
            'data': {
                "person_infos": [
                    {
                        "id": each_tester.get("id"),
                        "pingying": each_tester.get("pingying"),
                        "dpt_name": each_tester.get("dpt_name"),
                        "tester_name": each_tester.get("tester_name")
                     } for each_tester in tester_list
                ]
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def departmentPersonDelete(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)

        # 人员id
        person_id = params.get("person_id", -1)

        person_roles = read_pkl_file(person_role_file, pkl_key_dict[person_role_file])
        # print('person_roles ==>', person_roles)

        if person_roles:
            if person_id in person_roles:
                code_type = "success"
                del person_roles[person_id]
                write_pkl_file(person_role_file, person_roles, key_word=pkl_key_dict[person_role_file])
            else:
                code_type = "value_not_exists"
        else:
            code_type = "success"
        json_returns = {
            'data': {
                "person_id": person_id,
                "person_name": person_roles[person_id]["person_name"] if person_roles and person_roles.get(person_id) else None,
                "operator": "admin",
                "create_time": serialize_json_date_type(datetime.now())
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns, code_type=code_type),
                                content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def authorityAdd(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 人员id
        person_id = params.get('person_id')
        # 账号密码
        password = params.get('password')
        # 账号名称
        account_name = params.get('account_name')
        # 账号角色 (root, admin, normal)
        account_role = params.get('account_role')
        # 权限范围 0,1,2
        authority_type = params.get('authority_type')

        # 对新添加的角色做查询与存储
        person_authorities: dict = read_pkl_file(authority_file, pkl_key_dict[authority_file])
        # print('person_authorities ==>', person_authorities)
        if person_authorities:
            if person_id in person_authorities:
                code_type = "duplicated"
            else:
                if person_id:
                    code_type = "success"
                    person_authorities.update(
                        {
                            person_id: {
                                "person_id": person_id,
                                "password": password,
                                "account_name": account_name,
                                "account_role": account_role,
                                "authority_type": authority_type,
                                "operator": "admin",
                                "create_time": serialize_json_date_type(datetime.now())
                            }
                        }
                    )
                    write_pkl_file(authority_file, pkl_content=person_authorities, key_word=pkl_key_dict[authority_file])

                else:
                    code_type = "value_invalid"
        else:
            code_type = "success"
            person_authorities = {}
            person_authorities[pkl_key_dict[authority_file]] = {
                person_id: {
                    "person_id": person_id,
                    "password": password,
                    "account_name": account_name,
                    "account_role": account_role,
                    "authority_type": authority_type,
                    "operator": "admin",
                    "create_time": serialize_json_date_type(datetime.now())
                }
            }
            write_pkl_file(authority_file, pkl_content=person_authorities, key_word='')

        json_returns = {
            'data': {
                "person_id": person_id,
                "account_name": account_name,
                "account_role": account_role_types[account_role]['name'],
                "authority_type": authority_type,
                "operator": "admin",
                "create_time": serialize_json_date_type(datetime.now())
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns, code_type=code_type), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')

async def authoritySet(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 账号id
        person_id = params.get('person_id')
        # 账号名称
        account_name = params.get('account_name')
        # 账号密码
        password = params.get('password')
        # 账号角色 (root, admin, normal)
        account_role = params.get('account_role')
        # 权限范围 0,1,2
        authority_type = params.get('authority_type')

        if account_name or password or person_id > 0 or account_role or authority_type > 0:
            person_authorities: dict = read_pkl_file(authority_file, pkl_key_dict[authority_file])
            # print('person_authorities ==>', person_authorities)
            if person_id in person_authorities:
                if account_name:
                    person_authorities[person_id]['account_name'] = account_name
                if password:
                    person_authorities[person_id]['password'] = password
                if account_role:
                    person_authorities[person_id]['account_role'] = account_role
                if authority_type > 0:
                    person_authorities[person_id]['authority_type'] = authority_type
                # -- updaste -- 这里对创建时间做更新
                person_authorities[person_id]['create_time'] = serialize_json_date_type(datetime.now())
                code_type = "success"
                write_pkl_file(authority_file, pkl_content=person_authorities, key_word=pkl_key_dict[authority_file])
            else:
                code_type = "value_not_exists"
        else:
            code_type = "success"

        # 获取人员账号 --global--
        person_info_map = {each['user_id']: each['realname'] for each in cursor.search_alone(person_info_list_sql)}
        json_returns = {
            'data': {
                "person_id": person_id,
                "account_name": person_authorities[person_id]["account_name"] if person_authorities and person_authorities.get(
                    person_id) else None,
                "person_name": person_info_map[person_id]if person_authorities and person_authorities.get(
                    person_id) else None,
                "operator": "admin",
                "create_time": serialize_json_date_type(datetime.now())
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns, code_type=code_type),
                                content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def authorityDelete(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)

        # 人员id
        person_id = params.get("person_id", -1)

        person_authorities = read_pkl_file(authority_file, pkl_key_dict[authority_file])
        # print('person_authorities ==>', person_authorities)

        if person_authorities:
            # 查找到并做删除
            if person_id in person_authorities:
                code_type = "success"
                person_info = person_authorities.pop(person_id)
                write_pkl_file(authority_file, person_authorities, key_word=pkl_key_dict[authority_file])
            else:
                code_type = "value_not_exists"
        # 这里表示不用删除
        else:
            code_type = "success"
            person_info = {}
        json_returns = {
            'data': {
                "person_id": person_id,
                "person_name": person_info["account_name"] if person_info else None,
                "operator": "admin",
                "create_time": serialize_json_date_type(datetime.now())
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns, code_type=code_type),
                                content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def authorityShow(request):
    if request.method == 'GET':
        # 账号名称
        account_name = request.GET.get('account_name', "")
        # 姓名id
        person_id = int(request.GET.get('person_id', -1)) if request.GET.get('person_id', -1) else -1
        # 角色
        account_role = request.GET.get('account_role', "")

        # 所有类型的权限名称映射表
        account_roles_map = {typ: type_info['name'] for typ, type_info in account_role_types.items()}
        # 获取人员账号 --global--
        person_info_map = {each['user_id']: each['realname'] for each in cursor.search_alone(person_info_list_sql)}

        person_authorities = read_pkl_file(authority_file, pkl_key_dict[authority_file])
        # 对结果做过滤
        person_return_info = {}
        json_returns = {}
        # 判断是否已经判断过其他的条件
        other_condition_checked = False
        # 有查询条件
        if person_id > 0 or account_name != '""' or account_role != '""':
            # 遍历所有人员权限信息
            for each_person_id, each_authority in person_authorities.items():
                # 对 账号名称 查询条件1 和 账号名称 查询条件2 同时做判断如果存在则结束
                if (account_role != '""' and each_authority['account_role'] == account_role) and (account_name != '""' and each_authority['account_name'] == account_name):
                    person_return_info.update({each_person_id: each_authority})
                    break
                # 只有对 账号名称 查询条件2
                elif account_name != '""' and each_authority['account_name'] == account_name:
                    # 如果有条件1，但是当前不满足条件1的，直接返回空
                    if account_role != '""' and each_authority['account_role'] != account_role:
                        # 这里对空集合做解释，方便后面识别空的来源
                        other_condition_checked = True
                        # person_return_info = {}
                    # 如果没有条件1，则做存储
                    else:
                        person_return_info.update({each_person_id: each_authority})
                    break
                # 只有条件2，就只做存储
                elif account_role != '""' and each_authority['account_role'] == account_role:
                    if account_name != '""' and each_authority['account_name'] != account_name:
                        other_condition_checked = True
                    else:
                        person_return_info.update({each_person_id: each_authority})

            # 对 姓名id 查询条件0 做处理
            if person_id > 0:
                # 条件0 与其他条件的共同匹配
                if person_id in person_return_info:
                    person_return_info = {person_id: person_return_info[person_id]}
                # 条件0 与其他条件的不匹配: 1）其他条件判断后有值，但是与person_id不匹配，2）其他条件判断就不匹配，则不用判断条件0
                elif person_return_info or other_condition_checked:
                    person_return_info = {}
                # 只有条件0： 这里的为空，是因为只有此条件0，其他条件都没有
                elif not person_return_info and person_id in person_authorities:
                    person_return_info.update({person_id: person_authorities.get(person_id)})
                else:
                    person_return_info = {}

            if person_return_info:
                json_returns = {
                    'data': {
                        "person_infos": [
                            {
                                # 账号id
                                "person_id": each_person_authority.get('person_id'),
                                # 人员名称
                                "person_name": person_info_map.get(each_person_authority.get('person_id')),
                                # 密码
                                "password": each_person_authority.get('password'),
                                # 账号名称
                                "account_name": each_person_authority.get('account_name'),
                                # 账号角色类型
                                "account_role_type": each_person_authority.get('account_role'),
                                # 账号角色名称
                                "account_role": account_role_types.get(each_person_authority.get('account_role')).get('name') if account_role_types.get(each_person_authority.get('account_role')) else None,
                                # 权限范围
                                "authority_type": each_person_authority.get('authority_type'),
                                # 创建人
                                "operator": each_person_authority.get('operator'),
                                # 创建时间
                                "create_time": each_person_authority.get('create_time')
                            } for each_person_authority in person_return_info.values()
                        ]
                    }
                }
            else:
                json_returns = {
                    'data': {
                        "person_infos": []
                    }
                }

        # 无查询条件
        else:
            # print('person_authorities ==>', person_authorities)
            if person_authorities:
                json_returns = {
                    'data': {
                        "person_infos": [
                            {
                                # 账号id
                                "person_id": every_person_authority.get('person_id'),
                                # 人员名称
                                "person_name": person_info_map.get(every_person_authority.get('person_id')),
                                # 密码
                                "password": every_person_authority.get('password'),
                                # 账号名称
                                "account_name": every_person_authority.get('account_name'),
                                # 账号角色类型
                                "account_role_type": every_person_authority.get('account_role'),
                                # 账号角色
                                "account_role": account_role_types.get(every_person_authority.get('account_role')).get('name') if account_role_types.get(every_person_authority.get('account_role')) else None,
                                # 权限范围
                                "authority_type": every_person_authority.get('authority_type'),
                                # 创建人
                                "operator": every_person_authority.get('operator'),
                                # 创建时间
                                "create_time": every_person_authority.get('create_time')
                            } for every_person_authority in person_authorities.values()
                        ]
                    }
                }
            else:
                json_returns = {}

        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def mapDormainProgramIdShow(request):
    if request.method == 'GET':
        # 查询测试人员名单
        # tester_name_list = cursor.search_alone(department_person_info_list_sql)
        with open(department_person_file, 'rb') as f:
            tester_name_list = pickle.load(f)

        json_returns = {
            'data': {
                "person_infos": [
                    {
                        "id": each_tester.get("id"),
                        "pingying": each_tester.get("pingying"),
                        "dpt_name": each_tester.get("dpt_name"),
                        "tester_name": each_tester.get("tester_name")
                     } for each_tester in tester_name_list.get('person_infos')
                ]
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def positionDormainProgramIdShow(request):
    if request.method == 'GET':
        # 查询测试人员名单
        # tester_name_list = cursor.search_alone(department_person_info_list_sql)
        with open(department_person_file, 'rb') as f:
            tester_name_list = pickle.load(f)

        json_returns = {
            'data': {
                "person_infos": [
                    {
                        "id": each_tester.get("id"),
                        "pingying": each_tester.get("pingying"),
                        "dpt_name": each_tester.get("dpt_name"),
                        "tester_name": each_tester.get("tester_name")
                     } for each_tester in tester_name_list.get('person_infos')
                ]
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def deliverDormainProgramIdShow(request):
    if request.method == 'GET':
        # 查询测试人员名单
        # tester_name_list = cursor.search_alone(department_person_info_list_sql)
        with open(department_person_file, 'rb') as f:
            tester_name_list = pickle.load(f)

        json_returns = {
            'data': {
                "person_infos": [
                    {
                        "id": each_tester.get("id"),
                        "pingying": each_tester.get("pingying"),
                        "dpt_name": each_tester.get("dpt_name"),
                        "tester_name": each_tester.get("tester_name")
                     } for each_tester in tester_name_list.get('person_infos')
                ]
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def departmentList(request):
    if request.method == 'GET':
        # 查询测试人员名单
        department_info_list = cursor.search_alone(department_info_sql)

        json_returns = {
            "data": {
                "department_info_list": [
                    {
                        "id": dept['dept_id'],
                        "name": dept['dept_name'],
                    } for dept in department_info_list
                ]
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def personList(request):
    if request.method == 'GET':
        # 查询测试人员名单
        person_info_list = cursor.search_alone(person_info_list_sql)

        json_returns = {
            "data": {
                "person_info_list": [
                    {
                        "id": person['user_id'],
                        "name": person['realname'],
                        "account":  person['account'],
                    } for person in person_info_list
                ]
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def personRoleList(request):
    if request.method == 'GET':
        # 查询测试人员名单
        person_role_info_list = cursor.search_alone(person_role_info_list_sql)

        json_returns = {
            "data": {
                "person_info_list": [
                    {
                        "id": person['id'],
                        "name": person['name'],
                        "role": person['role'],
                        "description": person['description'],
                    } for person in person_role_info_list
                ]
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def accountList(request):
    if request.method == 'GET':
        # 查询测试人员名单
        account_name_list = cursor.search_alone(account_name_list_sql)

        json_returns = {
            "data": {
                "account_name_list": [
                    {
                        "id": account['user_id'],
                        "account":  account['account'],
                        "name": account['realname']
                    } for account in account_name_list
                ]
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def accountRoleList(request):
    if request.method == 'GET':
        # 查询测试人员名单
        # person_role_info_list = cursor.search_alone(person_role_info_list_sql)

        json_returns = {
            "data": {
                "account_role_list": [
                    {
                        "type": each_type,
                        "name": each_item['name'],
                        "priv_code": each_item['priv_code']
                    } for each_type, each_item in account_role_types.items()
                ]
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


