from django.contrib import admin
from django.urls import path
from configData import views

urlpatterns = [
    # 权限管理-角色管理
    path('departmentPersonShow', views.departmentPersonShow),
    path('departmentPersonAdd', views.departmentPersonAdd),
    path('departmentPersonSet', views.departmentPersonSet),
    path('departmentPersonDelete', views.departmentPersonDelete),
    # 权限管理-权限管理
    path('authorityAdd', views.authorityAdd),
    path('authoritySet', views.authoritySet),
    path('authorityDelete', views.authorityDelete),
    path('authorityShow', views.authorityShow),
    # 权限管理-下拉列表
    path('departmentList', views.departmentList),   # 权限管理-下拉列表-部门名称
    path('personList', views.personList),           # 权限管理-下拉列表-人员名称
    path('personRoleList', views.personRoleList),   # 权限管理-下拉列表-人员角色
    path('accountList', views.accountList),         # 权限管理-下拉列表-账号名称
    path('accountRoleList', views.accountRoleList), # 权限管理-下拉列表-账号角色

    # # 项目板块设置
    # path('mapDormainProgramIdSet', views.mapDormainProgramIdSet),
    # path('positionDormainProgramIdSet', views.positionDormainProgramIdSet),
    # path('deliverDormainProgramIdSet', views.deliverDormainProgramIdSet),
    # # 项目板块展示
    path('mapDormainProgramIdShow', views.mapDormainProgramIdShow),
    path('positionDormainProgramIdShow', views.positionDormainProgramIdShow),
    path('deliverDormainProgramIdShow', views.deliverDormainProgramIdShow),

]
