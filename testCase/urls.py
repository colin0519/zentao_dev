from django.contrib import admin
from django.urls import path
from testCase import views

urlpatterns = [
    path('caseExecuteTotal', views.caseExecuteTotal),
    path('personProjectSubsysCaseExecuteTotal', views.personProjectSubsysCaseExecuteTotal),
    path('caseTypeList', views.caseTypeList),
    path('projectList', views.projectList),
    path('creatorList', views.creatorList),
    path('caseExecuteTotal', views.caseExecuteTotal),
    path('personProjectSubsysCaseCreateTotal', views.personProjectSubsysCaseCreateTotal),
    path('executeResultList', views.executeResultList),
    path('caseFromList', views.caseFromList),
    path('requirementsNameList', views.requirementsNameList),
    path('outputCaseExecute', views.outputCaseExecute),
    path('outputCaseCreate', views.outputCaseCreate),
    path('creatorList', views.creatorList),
    path('assignerList', views.assignerList),
    path('caseIdList', views.caseIdList),
    path('caseStatistics', views.caseStatistics),
    path('caseCreateStatistics', views.caseCreateStatistics),
    # 项目时间段 分布测试用例执行数据
    path('executeTimeSlotShowByProject', views.executeTimeSlotShowByProject),
    # 项目日期 分布测试用例执行数据
    path('executeDateShowByProject', views.executeDateShowByProject),
    # 项目日期 分布测试用例创建数据
    path('createDateShowByDateProject', views.createDateShowByDateProject),

    # 迭代版本列表
    path('iterationList', views.iterationList),
    # 版本列表
    path('versionList', views.versionList),
    # 项目列表
    path('projectList', views.projectList),
    # 子系统列表
    path('subsysList', views.subsysList),
    # 测试单列表
    path('testSheetList', views.testSheetList),
    # 执行人名称
    path('executorList', views.executorList),
    # 用例执行-日期分布展示
    path('executeDateShow', views.executeDateShow),
    # 用例执行-时间段分布展示
    path('executeTimeSlotShow', views.executeTimeSlotShow),

    path('createDateShowByDate', views.createDateShowByDate),

    path('executeRestultList', views.executeRestultList),

    path('caseStatuslist', views.caseStatuslist),

]
