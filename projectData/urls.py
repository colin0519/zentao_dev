from django.contrib import admin
from django.urls import path
from projectData import views

urlpatterns = [
    path('projectIterationExtension', views.projectIterationExtension),
    path('subsystemIterationExtension', views.subsystemIterationExtension),
    path('subsystemBugPlot', views.subsystemBugPlot),
    path('subsystemIterationPlot', views.subsystemIterationPlot),
    path('subsystemIterationVersionExtension', views.subsystemIterationVersionExtension),
    path('IterationBugPlot', views.IterationBugPlot),
    path('IterationCasePlot', views.IterationCasePlot)
]
