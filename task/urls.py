from django.contrib import admin
from django.urls import path
from task import views

urlpatterns = [
    # 任务详情-项目列表
    path('projectList', views.projectList),
    # 任务详情-迭代列表
    path('iterationList', views.iterationList),
    # 任务详情-任务列表
    path('taskList', views.taskList),
    # 任务详情-状态列表
    path('statusList', views.statusList),
    # 任务详情-姓名列表
    path('nameList', views.nameList),
    # 任务详情-人员任务项清单
    path('personTaskDetailList', views.personTaskDetailList),

    # 任务汇总-部门列表
    path('departmentNameList', views.departmentNameList),

    # 任务汇总 - 任务类型
    path('taskTypeList', views.taskTypeList),
    # 任务汇总 - 任务项列表
    path('taskSummaryItemList', views.taskSummaryItemList),
]
