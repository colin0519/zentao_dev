import os
import pickle
import time
from collections import OrderedDict
import datetime

from common.constant import unsovleDailyBug_file
from common.sql_instance_project import today_unsolved_bug_sql
from utils.cursorHandler import cursor
from utils.normal_simple_function import get_file_dirname_path


def myprint():
    print(time.localtime())


def saveTodayUnsovledBugs():
    """
        用于存储每天的未解决Bug总数
    """
    today_unsolved_num = cursor.search_alone(today_unsolved_bug_sql)[0].get('count')
    current_day = str(datetime.date.today())
    bug_file = get_file_dirname_path(__file__, par_dir=True, concat_file=unsovleDailyBug_file)
    if not os.path.exists(bug_file):
        data_dot = OrderedDict()
    else:
        with open(bug_file, 'rb') as f:
            try:
                data_dot = pickle.load(f)
                print('data_dot=>', data_dot)
                # 只保留最近一个月的数据
                if len(data_dot) > 30:
                    data_dot.popitem()
            except EOFError:
                return None
    # 写入
    with open(bug_file, 'wb') as f:
        data_dot.update(
            {current_day: today_unsolved_num}
        )
        pickle.dump(data_dot, f)


if __name__ == "__main__":
    saveTodayUnsovledBugs()