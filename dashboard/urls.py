from django.contrib import admin
from django.urls import path
from dashboard import views

urlpatterns = [
    path('historyBugPriority', views.historyBugPriority),
    path('rankBoard', views.rankBoard),
    path('historyBugDelay', views.historyBugDelay),
    path('BugData', views.BugData),
    path('dataOverview', views.dataOverview),
    path('IterationVersion', views.IterationVersion),
    path('iterationVersionExtension', views.iterationVersionExtension),
    path('BugDetailList', views.BugDetailList),

]
