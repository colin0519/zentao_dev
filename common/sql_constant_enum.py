"""
    本文档用于处理：在SQL中需要转换的字段的映射关系
"""

# 解决方案中文名与禅道类型对应字典
resolution_name_type: dict = {
    ("fixed","已修复"): "fixed",
    ("otrepro",'不存在'):"notrepro",
    ("external",'外部原因'): "external",
    ("willnotfix",'不予修复'): "willnotfix",
    ("bydesign",'设计如此'): 'bydesign',
    ("duplicate",'重复提交'): 'duplicate',
    ("tostory",'待解决'): "tostory"
}


resolution_name_type_corresponding_English: dict = {
    "fixed": "已修复",
    "otrepro": '不存在',
    "external": '外部原因',
    "willnotfix": '不予修复',
    "bydesign": '设计如此',
    "duplicate": '重复提交',
    "tostory": '待解决',
    "postponed": '延期解决'
}


resolution_name_type_corresponding_Chinese: dict = {
    "已修复": "fixed",
    '不存在': "otrepro",
    '外部原因': "external",
    '不予修复': "willnotfix",
    '设计如此': "bydesign",
    '重复提交': "duplicate",
    '待解决': "tostory",
    '延期解决': "postponed"
}


weekday_name_corresponding_Chinese: dict = {
    '1': '星期一',
    '2': '星期二',
    '3': '星期三',
    '4': '星期四',
    '5': '星期五',
    '6': '星期六',
    '0': '星期日',
    '7': '星期日'
}




# Bug标签页切换
Bug_tag_type_name_type = {
    0: '全部',
    1: '已激活',
    2: '已解决',
    3: '已关闭',
    4: '空'
}


# Bug状态列表
Bug_status_constant_list = [
    {'status': '关闭'},
    {'status': '已解决'},
    {'status': '激活'}
]


Bug_status_constant_responding_Chinese = {
    '关闭': 'closed',
    '已解决': 'resolved',
    '激活': 'active'
}

Bug_status_constant_responding_English = {
    'closed': '关闭',
    'resolved': '已解决',
    'active': '激活'
}


case_status_constant_list = [
    {'status': '正常'},
    {'status': '阻塞'},
    {'status': '研究中'},
]



case_status_constant_responding_Chinese = {
    '正常': 'normal',
    '阻塞': 'blocked',
    '研究中': 'researching'
}

case_status_constant_responding_English = {
    'normal': '正常',
    'blocked': '阻塞',
    'researching': '研究中'
}

case_type_constant_responding_English_all = {
    'feature': '功能测试',
    'SFYZ': '算法数据验证',
    'XTXN': '系统性能测试',
    'DTYZ': '地图数据验证',
    'PTYZ': '平台数据验证',
    'performance': '定位性能测试',
    'config': '配置相关',
    'install': '安装部署',
    'security': '安全相关',
    'interface': '接口测试',
    'unit': '单元测试',
    'other': '其他',
}

case_type_constant_responding_English = {
    'feature': '功能测试',
    'SFYZ': '数据验证',
}



case_type_constant_responding_Chinese = {
    '功能测试': 'feature',
    '算法数据验证': 'SFYZ',
    '系统性能测试': 'XTXN',
    '地图数据验证': 'DTYZ',
    '平台数据验证': 'PTYZ',
    '定位性能测试': 'performance',
    '配置相关': 'config',
    '安装部署': 'install',
    '安全相关': 'security',
    '接口测试': 'interface',
    '单元测试': 'unit',
    '其他': 'other'
}


# case_type_constant_responding_Chinese = {
# "平台数据验证": "PTYZ",
# "功能验证": "feature",
# "性能验证": "performance",
# "接口验证": "interface",
# "配置验证": "config",
# }
#
#
# case_type_constant_reponding_English = {
#     "PTYZ": "平台数据验证",
#     "feature": "功能验证",
#     "performance": "性能验证",
#     "interface": "接口验证",
#     "config": "配置验证"
# }

case_type_constant_list = [
    {"type": "功能测试"},
    {"type": "数据验证"},
]


# case_type_constant_list = [
#     {'type': '功能测试'},
#     {'type': '算法数据验证'},
#     {'type': '系统性能测试'},
#     {'type': '地图数据验证'},
#     {'type': '平台数据验证'},
#     {'type': '定位性能测试'},
#     {'type': '配置相关'},
#     {'type': '安装部署'},
#     {'type': '安全相关'},
#     {'type': '接口测试'},
#     {'type': '单元测试'},
#     {'type': '其他'},
# ]




case_execute_result_constant_list = [
    {'status': '通过'},
    {'status': '阻塞'},
    {'status': '失败'},
]


case_execute_result_constant_responding_Chinese = {
    '通过': 'pass',
    '阻塞': 'blocked',
    '失败': 'fail'
}


case_execute_result_constant_responding_English = {
    'pass': '通过',
    'blocked': '阻塞',
    'fail': '失败'
}

Bug_severity_types_English = {
    1: "P1",
    2: "P2",
    3: "P3",
    4: "P4"
}

Bug_severity_types_Chinese = {
    "P1": 1,
    "P2": 2,
    "P3": 3,
    "P4": 4
}

project_status_types_Chinese = {
    "已完成": "closed",
    "进行中": "doing",
    "挂起": "suspended",
    "未开始": "wait"
}

project_status_types_English = {
    "closed": "已完成",
    "doing": "进行中",
    "suspended": "挂起",
    "wait": "未开始"
}

# 任务状态
task_status_types_English = {
    "cancel": "已取消",
    "closed": "已关闭",
    "doing": "进行中",
    "done": "已完成",
    "pause": "已暂停",
    "wait": "未开始",
}


task_status_types_Chinese = {
    "已取消": "cancel",
    "已关闭": "closed",
    "进行中": "doing",
    "已完成": "done",
    "已暂停": "pause",
    "未开始": "wait",
}


# 日报任务日志类型
log_content_English = {
    "opened": "创建任务",
    "started": "启动任务",
    "finished": "完成任务",
    "closed": "关闭任务"
}

# 账号角色映射表
account_role_types = {
    "root":{
        "name": "超级管理员",
        "priv_code": 1
    },
    "admin":{
        "name": "管理员",
        "priv_code": 2
    },
    "normal":{
        "name": "普通账号",
        "priv_code": 3
    },
    "forbidden":{
        "name": "账号冻结",
        "priv_code": -1
    }
}

# 权限类型表
authority_type_map = {
    1:"仅查看测试类数据",
    2: "仅查看开发类数据",
    3: "可查看所有数据",
}

task_type_English = {
    "affair": "事务",
    "architecture_design": "架构设计",
    "check": "验收",
    "design": "详细设计",
    "devel": "开发",
    "discuss": "讨论",
    "end": "结项",
    "general_request": "概要需求分析",
    "misc": "其他",
    "plan": "项目策划",
    "pmo": "项目管理",
    "request": "详细需求分析",
    "study": "研究",
    "support": "支持",
    "test": "测试",
    "ui": "UI",
}

task_type_Chinese = {
    "事务": "affair",
    "架构设计": "architecture_design",
    "验收": "check",
    "详细设计": "design",
    "开发": "devel",
    "讨论": "discuss",
    "结项": "end",
    "概要需求分析": "general_request",
    "其他": "misc",
    "项目策划": "plan",
    "项目管理": "pmo",
    "详细需求分析": "request",
    "研究": "study",
    "支持": "support",
    "测试": "test",
    "UI": "ui",
}


# case标签页切换
case_tag_type_name_type = {
    0: ('全部', None),
    1: ('通过', 'pass'),
    2: ('失败', 'fail'),
    3: ('阻塞', 'block'), # 这里没有查到
    4: ('忽略', '') # 这里没有查看到
}

# Bug数据类型
Bug_data_type = {
    0: "Bug有效率",
    1: "Bug总数",
    2: "Bug状态",
    3: "Bug解决方案"
}
# case数据类型
case_data_type = {
    0: "用例创建总数",
    1: "用例执行总数",
    2: "用例执行结果"
}

case_download_corresponding = {
    "case_id": '用例编号',
    "title": '用例标题',
    "status": '用例状态',
    "priority": '优先级',
    "creator": '创建者',
    "result": '结果',
    "create_date": '创建日期',
    "task_name": '任务名称',
    "task_id": '任务id',
    "milestone": '里程碑/迭代版本',
    "pject_id": '项目id',
    "pject_name": '项目名称',
    "prod_name": '产品项目',
    "prod_id": '产品id'
}


bug_download_corresponding = {
    "bug_id": 'Bug编号',
    "title": '标题',
    "status": 'Bug状态',
    "priority": '优先级',
    "creator": '创建者',
    "resolution": '解决方案',
    "task_name": '任务名称',
    "task_id": '任务id',
    "milestone": '里程碑/迭代',
    "prod_name": '产品名称',
    "prod_id": '产品id',
    "create_date": '创建日期'
}

project_id_corresponding = {
    (32, '定位'): '定位',
    (777, '车端地图'): '车端地图',
    (1063, '中台'): '中台',
    (172, '平台算法'): '平台算法',
    (157, '终端算法'): '终端算法',
    (780, '车路协同'): '车路协同',
    (677, '数字底座'): '数字底座'
}



project_download_colum_corresponding = {
    "project_id": '项目id',
    "project_name": '项目名称',
    "iteration_num": '迭代数',
    "project_status_running": '进行中',
    "project_status_blocked": '阻塞中',
    "project_status_done": '已完成',
    "version_bug_num": '版本Bug数',
    "history_unsolved_bug_total": '历史未解决Bug总数',
    "project_progress": '进度',
}

bug_status_corresponding = {
    'unsolved': '未解决',
    'created': '已创建',
    'solved':'已解决',
    'closed': '已关闭'

}


subsys_download_colunm_correspondeing = {
    "subsystem_id": '子系统id',
    "subsystem_name": '子系统名称',
    "online_version": '线上版本',
    "current_test_version": '当前测试版本',
    "plan_deadline": '计划截止时间',
    "history_unsolved_bug_total": '历史未解决Bug总数',
    "this_day_bug_add": '当日新增',
    "test_leader": '测试负责人',
    "test_status": '状态',
    "remark": '备注',
}


version_download_colunm_correspondeing = {
    "version_id": '版本id',
    "version_name": '版本名称',
    "plan_cycle": '计划周期',
    "plan_date_duration": '计划时长',
    "dev_leader": '开发负责人',
    "test_leader": '测试负责人',
    "unsolved_bug_total": '未解决Bug数',
    "this_day_bug_add": '当日新增Bug数',
    "execute_progress": '执行进度',
    "execute_status": '状态',
    "remark": '备注',
}
