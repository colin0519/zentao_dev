"""
    本文档用于存储常量的配置

"""
import pickle

mysql_ip = '127.0.0.1'
mysql_port = 3306
mysql_user = 'root'
mysql_password = 'root'
mysql_database = 'zentao'

# mysql_ip = '172.16.122.68'
# mysql_port = 3306
# mysql_user = 'test'
# mysql_password = 'dpi@2021'
# mysql_database = 'zentao'


file_path_case = './case_file.csv'
file_path_project = './project_file.csv'
file_path_bug = './bug_file.csv'
file_path_subsys = './subsys_file.csv'
file_path_version = './version_file.csv'

# redis 数据时效设置
OneDay = 24 * 3600
HalfDay = 4 * 3600
OneHour = 3600
TenMinutes = 10 * 60


TEST_DEPARTMENT_ID = 63

response_code = {
    "success": {
        "code": 200,
        "msg": "success"
    },
    "duplicated": {
        "code": 605,
        "msg": "deplicated value operation(重复操作)"
    },
    "value_not_exists": {
        "code": 705,
        "msg": "value not found(值不存在)"
    },
    "value_invalid": {
        "code": 805,
        "msg": "value is invalid(值无效)"
    }
}





# 部门人员配置文件名
department_person_file = './static_file/department_person.pkl'

# 角色信息文件
person_role_file = './static_file/person_role_file.pkl'

# 权限管理文件
authority_file = './static_file/authority_file.pkl'

# unsovleDailyBug_file = './dashboardUnsovledBugFile.pkl' # 本地配置
unsovleDailyBug_file = './zentao_second_dev/dashboardUnsovledBugFile.pkl'  # 线上环境

pkl_key_dict = {
    department_person_file: 'person_infos',
    person_role_file: 'person_role_infos',
    authority_file: 'authority_infos',
}


# 定位项目集
position_program_ids = {
    (32, '定位'): '定位',
    (1062, '定位平台'): '定位平台',
    (1063, '中台'): '中台',
    (172, '平台算法'): '平台算法',
    (157, '终端算法'): '终端算法',
}

# 地图项目集
map_program_ids = {
    (677, '数字底座'): '数字底座',
    (780, '车路协同'): '车路协同',
    (777, '车端地图引擎部'): '车端地图引擎部',
    (778, '平台地图引擎部'): '平台地图引擎部',
}



# 交付项目集
deliver_program_ids = {
    (278, "136项目"): "136项目",
    (1181, "2021年新一代自动驾驶平台设计及开发 项目"): "2021年新一代自动驾驶平台设计及开发 项目",
    (424, "北京市北斗融合创新应用示范项目"): "北京市北斗融合创新应用示范项目",
    (1059, "服务测试项目"): "服务测试项目",
}



# 服务端口(内部调用)
port_inner = '8000'


# 本机用户名 (从linux 访问windows主机使用scp， windows目录以linux斜杠为准从/开始)
win_user = r'laptop-rmarr8k8\thinkbook'
win_password = '0519'




if __name__ == "__main__":
    import os.path

    person_role_file = "." + person_role_file

    if not os.path.exists(person_role_file):
        with open(person_role_file, 'wb') as file:
            # pickle.dump({},file)
            pass