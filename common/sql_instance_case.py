"""
        本文档用于查询所有与用例相关的SQL语句
"""
# 查询所有的项目组id

postion_project_id_names = """
	SELECT
		id,
		(
			SELECT
				CASE pject.`name`
			WHEN "高精度定位平台" THEN
				"定位部"
			WHEN "中台技术" THEN
				"中台部"
			WHEN "平台算法部" THEN
				"平台算法"
			WHEN "卫惯组合终端算法" THEN
				"终端算法"
			END
		) AS NAME
	FROM
		zentao.zt_project AS pject
	WHERE
		pject. NAME IN (
			"高精度定位平台",
			"中台技术",
			"平台算法部",
			"卫惯组合终端算法"
		)
	and
	    deleted="0";
"""


# id in (32,144,172,779)

map_project_id_names = """
SELECT
		id,
		(
			SELECT
				CASE pject.`name`
			WHEN "车路协同平台部" THEN
			    "车路协同"
			WHEN "平台地图引擎部" THEN
			    "平台地图"
			WHEN "车端地图引擎部" THEN
			    "车端地图"
			END
		)
		as name
FROM
		zentao.zt_project AS pject
WHERE
		pject.name IN (
 				"车路协同平台部",
 				"平台地图引擎部",
				"车端地图引擎部"
					)
	and
	    deleted="0";
"""


deliver_project_id_names = """
SELECT
		id,`name`
FROM
		zentao.zt_project AS pject
WHERE
		pject.name IN (
 				"智能网联汽车大数据云控基础平台项目(136)",
 				"平台部",
				"奇瑞监控显示平台v1.1.0"
					)
AND
 		pject.type in ("program", "project");
"""



statistics_project_status = """
select 
	ttk.status, count(*)
-- ttk.name, ttk.`status`
from zt_testtask as ttk
WHERE 
    project
in 
(
    select 
        id
    from 
        zt_project
    where 
        parent = "%s");
    and 
	    deleted="0";
    group by 
        ttk.STATUS;
"""