from django.contrib import admin
from django.urls import path
from executeStatistics import views

urlpatterns = [
    path('caseStatistics', views.caseStatistics),
    path('testerNameList', views.testerNameList),
    path('verisonNOList', views.verisonNOList),
    path('subsysList', views.subsysList),
    path('projectList', views.projectList),
    path('creatorList', views.creatorList),
    path('executorList', views.executorList),
    path('caseStatuslist', views.caseStatuslist),
    path('executeRestultList', views.executeRestultList),
    path('testSheetList', views.testSheetList),
    path('executeDateShow', views.executeDateShow),
    path('executeTimeSlotShow', views.executeTimeSlotShow),
    path('createDateShow', views.createDateShow),
    path('createDateShowByDate', views.createDateShowByDate),
    path('caseTypeList', views.caseTypeList),

]
