from django.contrib import admin
from django.urls import path
from defect import views

urlpatterns = [
    # path('BugCreateTotal', views.BugCreateTotal),
    path('personProjectSubsysBugCreateTotal', views.personProjectSubsysBugCreateTotal), # 项目人员Bug创建数据
    path('creatorList', views.creatorList),
    path('projectStatus', views.projectStatus),
    path('severityList', views.severityList),
    path('outputBugVerifyData', views.outputBugVerifyData),
    path('outputBugCreateData', views.outputBugCreateData),
    path('outputBugCloseData', views.outputBugCloseData),
    path('BugVerifyTotal', views.BugVerifyTotal),
    path('personProjectSubsysBugVerifyTotal', views.personProjectSubsysBugVerifyTotal), # 项目人员Bug验证数据
    path('personProjectSubsysBugCloseTotal', views.personProjectSubsysBugCloseTotal),
    # path('BugToVerifyTotal', views.BugToVerifyTotal),
    path('personProjectSubsysBugToVerifyTotal', views.personProjectSubsysBugToVerifyTotal), # 项目人员Bug待验证数据
    # path('BugToSovledTotal', views.BugToSovledTotal),
    path('personProjectSubsysBugToSolvedTotal', views.personProjectSubsysBugToSolvedTotal), # 项目人员Bug待解决数据
    path('personProjectSubsysBugReOpenedTotal', views.personProjectSubsysBugReOpenedTotal), # 项目人员Bug激活数据
    path('BugDetailList', views.BugDetailList), # 缺陷详情列表

    path('projectList', views.projectList), # 项目列表

    path('subsysList', views.subsysList), # 子系统列表

    path('assignerList', views.assignerList), # 指派人列表
    path('BugstatusList', views.BugstatusList), # Bug状态列表
    path('BugResolutionList', views.BugResolutionList), # Bug解决方案列表
]
