import json

from asgiref.sync import sync_to_async
from django.http import HttpResponse, HttpResponseBadRequest
# Create your views here.
from common.sql_constant_enum import Bug_severity_types_Chinese, resolution_name_type_corresponding_Chinese, \
    Bug_status_constant_responding_Chinese, project_status_types_Chinese, project_status_types_English, \
    Bug_status_constant_responding_English, \
    resolution_name_type_corresponding_English, Bug_severity_types_English, Bug_status_constant_list, \
    resolution_name_type
from common.sql_instance_config import department_person_info_list_sql
from common.sql_instance_defect import *
from utils.cursorHandler import cursor, CursorHandler
from utils.normal_simple_function import serialize_json_date_type, serialize_json_dumps, fetch_file_abs_path, \
    redis_data_get
from common.constant import department_person_file, HalfDay


async def XX_BugCreateTotal(requests):
    return HttpResponse()


async def personProjectSubsysBugCreateTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 项目ids
        project_id = params.get('project_id', None)
        # Bug创建人
        creator = params.get('creator', None)
        # Bug创建起始时间
        date_create_time_start = params.get('date_create_time_start', None)
        # Bug创建终止时间
        date_create_time_end = params.get('date_create_time_end', None)
        # 严重等级
        severity = params.get('severity', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 按照数据类型分类
        Bug_create_statistics_sql = construct_person_system_subsys_create_bug_statistics(
            _testers=tuple(creator),
            _project_ids=tuple(project_id),
            _date_start_time=date_create_time_start,
            _date_end_time=date_create_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity), # 备用：前端页面显示的是中文
            # _severity=tuple(each for each in severity),
            _page_No=pageNum,
            _page_size=pageSize)
        print(Bug_create_statistics_sql)
        person_Bug_create_search_result = cursor.search_alone(Bug_create_statistics_sql)
        print(person_Bug_create_search_result[:2])

        Bug_create_sql_all = construct_person_system_subsys_create_bug_all(
            _testers=tuple(creator),
            _project_ids=tuple(project_id),
            _date_start_time=date_create_time_start,
            _date_end_time=date_create_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity)  # 备用：前端页面显示的是中文
            # _severity=tuple(each for each in severity),
        )
        print(Bug_create_sql_all)
        person_Bug_create_all = cursor.search_alone(Bug_create_sql_all)[0]

        # 用于结果挂载
        person_Bug_list = []
        # 人员项目数据集合
        person_data_dict = {}
        # 人员判断是否已经聚合过
        each_tester_name_set = set()
        # 项目id判断
        project_ids_tmp = set()
        # 统计每个人的所有的Bug
        person_total_Bug = 0

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))
        # 遍历返回的所有的测试人员与子系统Bug数
        # for each_person_subsys in  person_Bug_create_search_result:
        #     tester = each_person_subsys['tester']
        #     project_name = each_person_subsys['project_name']
        #     project_id = each_person_subsys['project_id']
        #     subsys_id = each_person_subsys['subsys_id']
        #     subsys_name = each_person_subsys['subsys_name']
        #     total = each_person_subsys['total']
        #     # 过滤掉非测试人员
        #     if tester not in tester_standard_set:
        #         continue
        #     if tester not in each_tester_name_set:
        #         # 新的测试人员数据，则初始化项目容器
        #         project_ids_tmp = set()
        #         # 判断是否已有上个数据结果
        #         if person_data_dict:
        #             person_data_dict['person_total_Bug'] = person_total_Bug
        #             person_Bug_list.append(person_data_dict)
        #             # 清空个人数据结果
        #             person_data_dict = {}
        #         person_data_dict['person_name'] = tester
        #         person_data_dict['project_Bug_list'] = []
        #         # 加入到项目id池中
        #         project_ids_tmp.add(project_id)
        #         # 初始化Bug总数
        #         project_Bug_nums = 0
        #         # 初始化返回数据结构
        #         person_data_dict['project_Bug_list'].append(
        #             {
        #                 "project_id": project_id,
        #                 "project_name": project_name,
        #                 "Bug_num": project_Bug_nums,
        #                 "subsys_Bug_list": [
        #                     {
        #                         "subsys_id": subsys_id,
        #                         "subsys_name": subsys_name,
        #                         "Bug_num": total,
        #                     }
        #                 ]
        #              }
        #         )
        #         # 此测试人员的信息做更新
        #         each_tester_name_set.add(tester)
        #     # 处理项目与子系统的数据
        #     else:
        #         # 没有统计过的项目，统计项目与子系统
        #         if project_id not in project_ids_tmp:
        #             project_ids_tmp.add(project_id)
        #             # 初始化Bug总数
        #             project_Bug_nums = 0
        #             person_data_dict['project_Bug_list'].append(
        #                 {
        #                     "project_id": project_id,
        #                     "project_name": project_name,
        #                     "Bug_num": project_Bug_nums,
        #                     "subsys_Bug_list": [
        #                         {
        #                             "subsys_id": subsys_id,
        #                             "subsys_name": subsys_name,
        #                             "Bug_num": total,
        #                         }
        #                     ]
        #                  }
        #             )
        #
        #         # 已统计过的项目，统计子系统
        #         else:
        #             person_data_dict['project_Bug_list'][-1]['subsys_Bug_list'].append(
        #                 {
        #                     "subsys_id": subsys_id,
        #                     "subsys_name": subsys_name,
        #                     "Bug_num": total,
        #                 }
        #             )
        #     # 更新项目Bug数
        #     project_Bug_nums += total
        #     person_data_dict['project_Bug_list'][-1]['Bug_num'] = project_Bug_nums
        #     # 统计本人的所有Bug的数据结果更新
        #     person_total_Bug += project_Bug_nums
        # else:
        #     person_data_dict['person_total_Bug'] = person_total_Bug
        #     person_Bug_list.append(person_data_dict)
        # 组织结果数据
        # 返回数据结构构造
        # json_returns = {'data': {'person_bug_list': person_Bug_list}}
        json_returns = {'data': {'person_bug_list': person_Bug_create_search_result, 'total': person_Bug_create_all.get('total'),
                                 'date_create_time_start': date_create_time_start,
                                 'date_create_time_end': date_create_time_end,
                                 'severity': severity}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def XX_BugCreateTotal(request):
    return HttpResponse()


async def creatorList(request):
    if request.method == 'GET':
        creator_name_list = cursor.search_alone(Bug_creator_name_list_sql)
        print(creator_name_list)
        json_returns = {
            'data': {
                "creator_names": creator_name_list,
                'total': len(creator_name_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def projectStatus(request):
    if request.method == 'GET':
        json_returns = {
            'data': {
                "project_status": [
                    {
                        "status": each_status,
                        "name": status_name
                    } for each_status, status_name in project_status_types_English.items()
                ]
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def severityList(request):
    if request.method == 'GET':
        json_returns = {
            'data': {
                "severity_types": [
                    {
                        "severity": severity
                    } for severity in Bug_severity_types_Chinese.keys()
                ]
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def outputBugVerifyData(request):
    return HttpResponse()


async def outputBugCreateData(request):
    return HttpResponse()


async def outputBugCloseData(request):
    return HttpResponse()


async def BugVerifyTotal(request):
    return HttpResponse()


async def personProjectSubsysBugVerifyTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 项目ids
        project_id = params.get('project_id', None)
        # Bug创建人
        verifier = params.get('verifier', None)
        # Bug创建起始时间
        date_verify_time_start = params.get('date_verify_time_start', None)
        # Bug创建终止时间
        date_verify_time_end = params.get('date_verify_time_end', None)
        # 严重等级
        severity = params.get('severity', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 按照数据类型分类
        Bug_verify_statistics_sql = construct_person_system_subsys_verify_bug_statistics(
            _testers=tuple(verifier),
            _project_ids=tuple(project_id),
            _date_start_time=date_verify_time_start,
            _date_end_time=date_verify_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity),
            _page_No=pageNum,
            _page_size=pageSize)
        print(Bug_verify_statistics_sql)
        person_Bug_verify_search_result = cursor.search_alone(Bug_verify_statistics_sql)
        print('person_Bug_verify_search_result', person_Bug_verify_search_result[:2])

        Bug_verify_sql_all = construct_person_system_subsys_verify_bug_all(
            _testers=tuple(verifier),
            _project_ids=tuple(project_id),
            _date_start_time=date_verify_time_start,
            _date_end_time=date_verify_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity)
        )
        print(Bug_verify_sql_all)
        person_Bug_verify_all = cursor.search_alone(Bug_verify_sql_all)[0]

        # 用于结果挂载
        person_Bug_list = []
        # 人员项目数据集合
        person_data_dict = {}
        # 人员判断是否已经聚合过
        each_tester_name_set = set()
        # 项目id判断
        project_ids_tmp = set()
        # 统计每个人的所有的Bug
        person_total_Bug = 0

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))
        # # 遍历返回的所有的测试人员与子系统Bug数
        # for each_person_subsys in  person_Bug_verify_search_result:
        #     tester = each_person_subsys['tester']
        #     project_name = each_person_subsys['project_name']
        #     project_id = each_person_subsys['project_id']
        #     subsys_id = each_person_subsys['subsys_id']
        #     subsys_name = each_person_subsys['subsys_name']
        #     total = each_person_subsys['total']
        #     # 过滤掉非测试人员
        #     if tester not in tester_standard_set:
        #         continue
        #     if tester not in each_tester_name_set:
        #         # 新的测试人员数据，则初始化项目容器
        #         project_ids_tmp = set()
        #         # 判断是否已有上个数据结果
        #         if person_data_dict:
        #             person_data_dict['person_total_Bug'] = person_total_Bug
        #             person_Bug_list.append(person_data_dict)
        #             # 清空个人数据结果
        #             person_data_dict = {}
        #             person_total_Bug = 0
        #         person_data_dict['person_name'] = tester
        #         person_data_dict['project_Bug_list'] = []
        #         # 加入到项目id池中
        #         project_ids_tmp.add(project_id)
        #         # 初始化Bug总数
        #         project_Bug_nums = 0
        #         # 初始化返回数据结构
        #         person_data_dict['project_Bug_list'].append(
        #             {
        #                 "project_id": project_id,
        #                 "project_name": project_name,
        #                 "Bug_num": project_Bug_nums,
        #                 "subsys_Bug_list": [
        #                     {
        #                         "subsys_id": subsys_id,
        #                         "subsys_name": subsys_name,
        #                         "Bug_num": total,
        #                     }
        #                 ]
        #              }
        #         )
        #         # 此测试人员的信息做更新
        #         each_tester_name_set.add(tester)
        #     # 处理项目与子系统的数据
        #     else:
        #         # 没有统计过的项目，统计项目与子系统
        #         if project_id not in project_ids_tmp:
        #             project_ids_tmp.add(project_id)
        #             # 初始化Bug总数
        #             project_Bug_nums = 0
        #             person_data_dict['project_Bug_list'].append(
        #                 {
        #                     "project_id": project_id,
        #                     "project_name": project_name,
        #                     "Bug_num": project_Bug_nums,
        #                     "subsys_Bug_list": [
        #                         {
        #                             "subsys_id": subsys_id,
        #                             "subsys_name": subsys_name,
        #                             "Bug_num": total,
        #                         }
        #                     ]
        #                  }
        #             )
        #
        #         # 已统计过的项目，统计子系统
        #         else:
        #             person_data_dict['project_Bug_list'][-1]['subsys_Bug_list'].append(
        #                 {
        #                     "subsys_id": subsys_id,
        #                     "subsys_name": subsys_name,
        #                     "Bug_num": total,
        #                 }
        #             )
        #     # 更新项目Bug数
        #     project_Bug_nums += total
        #     person_data_dict['project_Bug_list'][-1]['Bug_num'] = project_Bug_nums
        #     # 统计本人的所有Bug的数据结果更新
        #     person_total_Bug += project_Bug_nums
        # else:
        #     person_data_dict['person_total_Bug'] = person_total_Bug
        #     person_Bug_list.append(person_data_dict)
        # 组织结果数据
        # 返回数据结构构造
        # json_returns = {'data': {'person_bug_list': person_Bug_list}}
        json_returns = {'data': {'person_bug_list': person_Bug_verify_search_result, 'total': person_Bug_verify_all,
                                 'date_verify_time_start': date_verify_time_start,
                                 'date_verify_time_end': date_verify_time_end,
                                 'severity': severity}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def XX_BugToVerifyTotal(request):
    return HttpResponse()


async def personProjectSubsysBugToVerifyTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 项目ids
        project_id = params.get('project_id', None)
        # Bug创建人
        verifier = params.get('verifier', None)
        # Bug创建起始时间
        date_verify_time_start = params.get('date_verify_time_start', None)
        # Bug创建终止时间
        date_verify_time_end = params.get('date_verify_time_end', None)
        # 严重等级
        severity = params.get('severity', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 按照数据类型分类
        Bug_to_verify_statistics_sql = construct_person_system_subsys_to_verify_bug_statistics(
            _testers=tuple(verifier),
            _project_ids=tuple(project_id),
            _date_start_time=date_verify_time_start,
            _date_end_time=date_verify_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity),
            _page_No=pageNum,
            _page_size=pageSize)
        print(Bug_to_verify_statistics_sql)
        person_Bug_verify_search_result = cursor.search_alone(Bug_to_verify_statistics_sql)
        print('person_Bug_verify_search_result', person_Bug_verify_search_result[:2])

        # 用于结果挂载
        person_Bug_list = []
        # 人员项目数据集合
        person_data_dict = {}
        # 人员判断是否已经聚合过
        each_tester_name_set = set()
        # 项目id判断
        project_ids_tmp = set()
        # 统计每个人的所有的Bug
        person_total_Bug = 0

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))
        # 遍历返回的所有的测试人员与子系统Bug数
        for each_person_subsys in person_Bug_verify_search_result:
            tester = each_person_subsys['tester']
            project_name = each_person_subsys['project_name']
            project_id = each_person_subsys['project_id']
            subsys_id = each_person_subsys['subsys_id']
            subsys_name = each_person_subsys['subsys_name']
            total = each_person_subsys['total']
            # 过滤掉非测试人员
            if tester not in tester_standard_set:
                continue
            if tester not in each_tester_name_set:
                # 新的测试人员数据，则初始化项目容器
                project_ids_tmp = set()
                # 判断是否已有上个数据结果
                if person_data_dict:
                    # person_data_dict['person_total_Bug'] = person_total_Bug
                    person_data_dict['person_total_Bug'] = sum([each_project['Bug_num'] for each_project in person_data_dict['project_Bug_list']])
                    person_Bug_list.append(person_data_dict)
                    # 清空个人数据结果
                    person_data_dict = {}
                    person_total_Bug = 0
                person_data_dict['person_name'] = tester
                person_data_dict['project_Bug_list'] = []
                # 加入到项目id池中
                project_ids_tmp.add(project_id)
                # 初始化Bug总数
                project_Bug_nums = 0
                # 初始化返回数据结构
                person_data_dict['project_Bug_list'].append(
                    {
                        "project_id": project_id,
                        "project_name": project_name,
                        "Bug_num": project_Bug_nums,
                        "subsys_Bug_list": [
                            {
                                "subsys_id": subsys_id,
                                "subsys_name": subsys_name,
                                "Bug_num": total,
                            }
                        ]
                    }
                )
                # 此测试人员的信息做更新
                each_tester_name_set.add(tester)
            # 处理项目与子系统的数据
            else:
                # 没有统计过的项目，统计项目与子系统
                if project_id not in project_ids_tmp:
                    project_ids_tmp.add(project_id)
                    # 初始化Bug总数
                    project_Bug_nums = 0
                    person_data_dict['project_Bug_list'].append(
                        {
                            "project_id": project_id,
                            "project_name": project_name,
                            "Bug_num": project_Bug_nums,
                            "subsys_Bug_list": [
                                {
                                    "subsys_id": subsys_id,
                                    "subsys_name": subsys_name,
                                    "Bug_num": total,
                                }
                            ]
                        }
                    )

                # 已统计过的项目，统计子系统
                else:
                    person_data_dict['project_Bug_list'][-1]['subsys_Bug_list'].append(
                        {
                            "subsys_id": subsys_id,
                            "subsys_name": subsys_name,
                            "Bug_num": total,
                        }
                    )
            # 更新项目Bug数
            project_Bug_nums += total
            person_data_dict['project_Bug_list'][-1]['Bug_num'] = project_Bug_nums
            # 统计本人的所有Bug的数据结果更新
            person_total_Bug += project_Bug_nums
        else:
            # person_data_dict['person_total_Bug'] = person_total_Bug
            person_data_dict['person_total_Bug'] = sum(
                [each_project['Bug_num'] for each_project in person_data_dict['project_Bug_list']] if person_data_dict else [0]
            )
            person_Bug_list.append(person_data_dict)
        # 组织结果数据
        # 返回数据结构构造
        json_returns = {'data': {'person_bug_list': person_Bug_list,
                                 'date_verify_time_start': date_verify_time_start,
                                 'date_verify_time_end': date_verify_time_end,
                                 'severity': severity}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def XX_BugToSovledTotal(request):
    return HttpResponse()


async def personProjectSubsysBugToSolvedTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 项目ids
        project_id = params.get('project_id', None)
        # Bug创建人
        verifier = params.get('verifier', None)
        # Bug创建起始时间
        date_verify_time_start = params.get('date_verify_time_start', None)
        # Bug创建终止时间
        date_verify_time_end = params.get('date_verify_time_end', None)
        # 严重等级
        severity = params.get('severity', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 按照数据类型分类
        Bug_to_sovled_statistics_sql = construct_person_system_subsys_to_solved_bug_statistics(
            _testers=tuple(verifier),
            _project_ids=tuple(project_id),
            _date_start_time=date_verify_time_start,
            _date_end_time=date_verify_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity),
            _page_No=pageNum,
            _page_size=pageSize)
        print(Bug_to_sovled_statistics_sql)
        person_Bug_verify_search_result = cursor.search_alone(Bug_to_sovled_statistics_sql)
        print('person_Bug_verify_search_result', person_Bug_verify_search_result[:2])

        # 用于结果挂载
        person_Bug_list = []
        # 人员项目数据集合
        person_data_dict = {}
        # 人员判断是否已经聚合过
        each_tester_name_set = set()
        # 项目id判断
        project_ids_tmp = set()
        # 统计每个人的所有的Bug
        person_total_Bug = 0

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))
        # 遍历返回的所有的测试人员与子系统Bug数
        for each_person_subsys in  person_Bug_verify_search_result:
            tester = each_person_subsys['tester']
            project_name = each_person_subsys['project_name']
            project_id = each_person_subsys['project_id']
            subsys_id = each_person_subsys['subsys_id']
            subsys_name = each_person_subsys['subsys_name']
            total = each_person_subsys['total']
            # 过滤掉非测试人员
            if tester not in tester_standard_set:
                continue
            if tester not in each_tester_name_set:
                # 新的测试人员数据，则初始化项目容器
                project_ids_tmp = set()
                # 判断是否已有上个数据结果
                if person_data_dict:
                    person_data_dict['person_total_Bug'] = sum([each_project['Bug_num'] for each_project in person_data_dict['project_Bug_list']])
                    person_Bug_list.append(person_data_dict)
                    # 清空个人数据结果
                    person_data_dict = {}
                    person_total_Bug = 0
                person_data_dict['person_name'] = tester
                person_data_dict['project_Bug_list'] = []
                # 加入到项目id池中
                project_ids_tmp.add(project_id)
                # 初始化Bug总数
                project_Bug_nums = 0
                # 初始化返回数据结构
                person_data_dict['project_Bug_list'].append(
                    {
                        "project_id": project_id,
                        "project_name": project_name,
                        "Bug_num": project_Bug_nums,
                        "subsys_Bug_list": [
                            {
                                "subsys_id": subsys_id,
                                "subsys_name": subsys_name,
                                "Bug_num": total,
                            }
                        ]
                     }
                )
                # 此测试人员的信息做更新
                each_tester_name_set.add(tester)
            # 处理项目与子系统的数据
            else:
                # 没有统计过的项目，统计项目与子系统
                if project_id not in project_ids_tmp:
                    project_ids_tmp.add(project_id)
                    # 初始化Bug总数
                    project_Bug_nums = 0
                    person_data_dict['project_Bug_list'].append(
                        {
                            "project_id": project_id,
                            "project_name": project_name,
                            "Bug_num": project_Bug_nums,
                            "subsys_Bug_list": [
                                {
                                    "subsys_id": subsys_id,
                                    "subsys_name": subsys_name,
                                    "Bug_num": total,
                                }
                            ]
                         }
                    )

                # 已统计过的项目，统计子系统
                else:
                    person_data_dict['project_Bug_list'][-1]['subsys_Bug_list'].append(
                        {
                            "subsys_id": subsys_id,
                            "subsys_name": subsys_name,
                            "Bug_num": total,
                        }
                    )
            # 更新项目Bug数
            project_Bug_nums += total
            person_data_dict['project_Bug_list'][-1]['Bug_num'] = project_Bug_nums
            # 统计本人的所有Bug的数据结果更新
            person_total_Bug += project_Bug_nums
        else:
            # person_data_dict['person_total_Bug'] = person_total_Bug
            person_data_dict['person_total_Bug'] = sum(
                [each_project['Bug_num'] for each_project in person_data_dict['project_Bug_list']] if person_data_dict else [0]
            )
            person_Bug_list.append(person_data_dict)
        # 组织结果数据
        # 返回数据结构构造
        json_returns = {'data': {'person_bug_list': person_Bug_list,
                                 'date_verify_time_start': date_verify_time_start,
                                 'date_verify_time_end': date_verify_time_end,
                                 'severity': severity}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugDetailList(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 项目id
        project_id = params.get('project_id', None)
        # 子系统id
        subsys_id = params.get('subsys_id', None)
        # 迭代id
        iteration_id = params.get('iteration_id', None)
        # 版本id
        version_id = params.get('version_id', None)
        # Bug创建人
        creator_name = params.get('creator_name', None)
        # 指派人
        assigner_name = params.get('assigner_name', None)
        # 创建起始日期
        date_create_start = params.get('date_create_start', None)
        # 创建截止日期
        date_create_end = params.get('date_create_end', None)
        # 解决的起始日期
        date_solve_start = params.get('date_solve_start', None)
        # 解决的截止日期
        date_solve_end = params.get('date_solve_end', None)
        # 解决方案
        solve_resolution = params.get('solve_resolution', None)
        # Bug状态
        Bug_status = params.get('Bug_status', None)
        # Bug验证等级
        Bug_severity = params.get('severity', None)
        # 项目状态
        project_status = params.get('project_status', None)
        # 第 n 页
        pageNo = params.get('pageNo', 0)
        # 显示数量
        pageNum = params.get('pageNum', 25)
        # Bug 条件查询的数据结果
        Bug_condition_search_result = []
        Bug_condition_search_sql = construct_sql_defect_statistics_detail(
                _project_id=project_id,
                _subsys_id=subsys_id,
                _iteration_id=iteration_id,
                _version_id=version_id,
                _creator_name=creator_name,
                _assigner_name=assigner_name,
                _date_create_start=date_create_start,
                _date_create_end=date_create_end,
                _date_solve_start=date_solve_start,
                _date_solve_end=date_solve_end,
                _solve_resolution=resolution_name_type_corresponding_Chinese.get(solve_resolution),
                _Bug_status=Bug_status_constant_responding_Chinese.get(Bug_status),
                # _Bug_severity=Bug_severity_types_Chinese.get(Bug_severity),
                _Bug_severity=tuple(Bug_severity_types_Chinese.get(each) for each in Bug_severity),
                _project_status=project_status_types_Chinese.get(project_status),
                _pageNo=pageNo,
                _pageNum=pageNum
        )
        print('--------------------------------->>>>>>> >>>>>', Bug_condition_search_sql)
        Bug_condition_search_all_sql = construct_sql_defect_statistics_all(
            _project_id=project_id,
            _subsys_id=subsys_id,
            _iteration_id=iteration_id,
            _version_id=version_id,
            _creator_name=creator_name,
            _assigner_name=assigner_name,
            _date_create_start=date_create_start,
            _date_create_end=date_create_end,
            _date_solve_start=date_solve_start,
            _date_solve_end=date_solve_end,
            _solve_resolution=resolution_name_type_corresponding_Chinese.get(solve_resolution),
            _Bug_status=Bug_status_constant_responding_Chinese.get(Bug_status),
            # _Bug_severity=Bug_severity_types_Chinese.get(Bug_severity),
            _Bug_severity=tuple(Bug_severity_types_Chinese.get(each) for each in Bug_severity),
            _project_status=project_status_types_Chinese.get(project_status)
        )
        print('Bug_condition_search_all_sql', Bug_condition_search_all_sql)
        Bug_condition_search_result = await sync_to_async(cursor.search_alone, thread_sensitive=True)(Bug_condition_search_sql)
        Bug_condition_search_all_result = await sync_to_async(cursor.search_alone, thread_sensitive=True)(Bug_condition_search_all_sql)
        # Bug_condition_search_result = cursor.search_alone(Bug_condition_search_sql)
        # Bug_condition_search_all_result = cursor.search_alone(Bug_condition_search_all_sql)[0].get('total')
        json_returns = {
            'data': {
                "Bug_total": Bug_condition_search_all_result[0].get('total'),  # Bug总数
                "project_id": project_id, # 项目id
                "subsys_id": subsys_id, # 子系统id
                "creator_name": creator_name, # Bug创建人
                "assigner_name": assigner_name, # 指派人
                "date_create_start": date_create_start, # 创建起始日期
                "date_create_end": date_create_end, # 创建截止日期
                "solve_resolution": solve_resolution, # 解决方案
                "date_solve_start": date_solve_start, # 解决的起始日期
                "date_solve_end": date_solve_end, # 解决的截止日期
                "Bug_status": Bug_status, # Bug状态
                "Bug_detail_list": [{
                    # BugId
                    "Bug_id": each_bug.get('bug_id'),
                    # 项目id
                    "project_id": each_bug.get('project_id'),
                    # 子系统id
                    "subsys_id": each_bug.get('subsys_id'),
                    # 迭代id
                    "iteration_id": each_bug.get('iteration_id'),
                    # 版本id
                    "version_id": each_bug.get('version_id'),
                    # 迭代名称
                    "iteration_name": each_bug.get('iteration_name'),
                    # 版本名称
                    "version_name": each_bug.get('version_name'),
                    # 项目名称
                    "project_name": each_bug.get('project_name'),
                    # 子系统名称
                    "subsys_name": each_bug.get('subsys_name'),
                    # Bug严重程度
                    "Bug_severity": Bug_severity_types_English.get(each_bug.get('severity')),
                    # Bug标题
                    "Bug_title": each_bug.get('title'),
                    # 创建人
                    "creator": each_bug.get('creator'),
                    # 创建时间
                    "create_date": serialize_json_date_type(each_bug.get('create_date')),
                    # 创建人
                    "assigner": each_bug.get('assigner'),
                    # 创建时间
                    "sovled_date": serialize_json_date_type(each_bug.get('sovled_date')),
                    # Bug状态
                    "Bug_status": Bug_status_constant_responding_English.get(each_bug.get('status')),
                    # 解决方案
                    "solve_resolution": resolution_name_type_corresponding_English.get(each_bug.get('resolution')),
                    # 项目状态
                    "project_status": project_status_types_English.get(each_bug.get("project_status")),
                } for each_bug in Bug_condition_search_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def personProjectSubsysBugCloseTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 项目ids
        project_id = params.get('project_id', None)
        # Bug创建人
        closer = params.get('closer', None)
        # Bug创建起始时间
        date_close_time_start = params.get('date_close_time_start', None)
        # Bug创建终止时间
        date_close_time_end = params.get('date_close_time_end', None)
        # 严重等级
        severity = params.get('severity', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 按照数据类型分类
        Bug_verify_statistics_sql = construct_person_system_subsys_closed_bug_statistics(
            _testers=tuple(closer),
            _project_ids=tuple(project_id),
            _date_start_time=date_close_time_start,
            _date_end_time=date_close_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity),
            _page_No=pageNum,
            _page_size=pageSize)
        print(Bug_verify_statistics_sql)
        person_Bug_close_search_result = cursor.search_alone(Bug_verify_statistics_sql)
        print('person_Bug_verify_search_result', person_Bug_close_search_result[:2])

        Bug_verify_sql_all = construct_person_system_subsys_closed_bug_all(
            _testers=tuple(closer),
            _project_ids=tuple(project_id),
            _date_start_time=date_close_time_start,
            _date_end_time=date_close_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity),
        )
        print(Bug_verify_sql_all)
        person_Bug_close_all = cursor.search_alone(Bug_verify_sql_all)[0]

        # 用于结果挂载
        person_Bug_list = []
        # 人员项目数据集合
        person_data_dict = {}
        # 人员判断是否已经聚合过
        each_tester_name_set = set()
        # 项目id判断
        project_ids_tmp = set()
        # 统计每个人的所有的Bug
        person_total_Bug = 0

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))
        # # 遍历返回的所有的测试人员与子系统Bug数
        # for each_person_subsys in  person_Bug_verify_search_result:
        #     tester = each_person_subsys['tester']
        #     project_name = each_person_subsys['project_name']
        #     project_id = each_person_subsys['project_id']
        #     subsys_id = each_person_subsys['subsys_id']
        #     subsys_name = each_person_subsys['subsys_name']
        #     total = each_person_subsys['total']
        #     # 过滤掉非测试人员
        #     if tester not in tester_standard_set:
        #         continue
        #     if tester not in each_tester_name_set:
        #         # 新的测试人员数据，则初始化项目容器
        #         project_ids_tmp = set()
        #         # 判断是否已有上个数据结果
        #         if person_data_dict:
        #             person_data_dict['person_total_Bug'] = person_total_Bug
        #             person_Bug_list.append(person_data_dict)
        #             # 清空个人数据结果
        #             person_data_dict = {}
        #             person_total_Bug = 0
        #         person_data_dict['person_name'] = tester
        #         person_data_dict['project_Bug_list'] = []
        #         # 加入到项目id池中
        #         project_ids_tmp.add(project_id)
        #         # 初始化Bug总数
        #         project_Bug_nums = 0
        #         # 初始化返回数据结构
        #         person_data_dict['project_Bug_list'].append(
        #             {
        #                 "project_id": project_id,
        #                 "project_name": project_name,
        #                 "Bug_num": project_Bug_nums,
        #                 "subsys_Bug_list": [
        #                     {
        #                         "subsys_id": subsys_id,
        #                         "subsys_name": subsys_name,
        #                         "Bug_num": total,
        #                     }
        #                 ]
        #              }
        #         )
        #         # 此测试人员的信息做更新
        #         each_tester_name_set.add(tester)
        #     # 处理项目与子系统的数据
        #     else:
        #         # 没有统计过的项目，统计项目与子系统
        #         if project_id not in project_ids_tmp:
        #             project_ids_tmp.add(project_id)
        #             # 初始化Bug总数
        #             project_Bug_nums = 0
        #             person_data_dict['project_Bug_list'].append(
        #                 {
        #                     "project_id": project_id,
        #                     "project_name": project_name,
        #                     "Bug_num": project_Bug_nums,
        #                     "subsys_Bug_list": [
        #                         {
        #                             "subsys_id": subsys_id,
        #                             "subsys_name": subsys_name,
        #                             "Bug_num": total,
        #                         }
        #                     ]
        #                  }
        #             )
        #
        #         # 已统计过的项目，统计子系统
        #         else:
        #             person_data_dict['project_Bug_list'][-1]['subsys_Bug_list'].append(
        #                 {
        #                     "subsys_id": subsys_id,
        #                     "subsys_name": subsys_name,
        #                     "Bug_num": total,
        #                 }
        #             )
        #     # 更新项目Bug数
        #     project_Bug_nums += total
        #     person_data_dict['project_Bug_list'][-1]['Bug_num'] = project_Bug_nums
        #     # 统计本人的所有Bug的数据结果更新
        #     person_total_Bug += project_Bug_nums
        # else:
        #     person_data_dict['person_total_Bug'] = person_total_Bug
        #     person_Bug_list.append(person_data_dict)
        # 组织结果数据
        # 返回数据结构构造
        # json_returns = {'data': {'person_bug_list': person_Bug_list}}
        json_returns = {'data': {'person_bug_list': person_Bug_close_search_result, 'total': person_Bug_close_all.get('total'),
                                 'date_close_time_start': date_close_time_start,
                                 'date_close_time_end': date_close_time_end,
                                 'severity': severity
                                 }}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def personProjectSubsysBugReOpenedTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 项目ids
        project_id = params.get('project_id', None)
        # Bug创建人
        verifier = params.get('verifier', None)
        # Bug创建起始时间
        date_verify_time_start = params.get('date_verify_time_start', None)
        # Bug创建终止时间
        date_verify_time_end = params.get('date_verify_time_end', None)
        # 严重等级
        severity = params.get('severity', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 按照数据类型分类
        Bug_Reopen_statistics_sql = construct_person_system_subsys_reopened_bug_statistics(
            _testers=tuple(verifier),
            _project_ids=tuple(project_id),
            _date_start_time=date_verify_time_start,
            _date_end_time=date_verify_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity),
            _page_No=pageNum,
            _page_size=pageSize)
        print(Bug_Reopen_statistics_sql)
        person_Bug_Reopen_search_result = cursor.search_alone(Bug_Reopen_statistics_sql)
        print('person_Bug_Reopen_search_result', person_Bug_Reopen_search_result[:2])

        Bug_Reopen_statistics_sql_all = construct_person_system_subsys_reopened_bug_all(
            _testers=tuple(verifier),
            _project_ids=tuple(project_id),
            _date_start_time=date_verify_time_start,
            _date_end_time=date_verify_time_end,
            _severity=tuple(Bug_severity_types_Chinese.get(each) for each in severity)
        )
        person_Bug_Reopen_all = cursor.search_alone(Bug_Reopen_statistics_sql_all)[0]


        # 用于结果挂载
        person_Bug_list = []
        # 人员项目数据集合
        person_data_dict = {}
        # 人员判断是否已经聚合过
        each_tester_name_set = set()
        # 项目id判断
        project_ids_tmp = set()
        # 统计每个人的所有的Bug
        person_total_Bug = 0

        # # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))


        # # 遍历返回的所有的测试人员与子系统Bug数
        # for each_person_subsys in  person_Bug_Reopen_search_result:
        #     tester = each_person_subsys['tester']
        #     project_name = each_person_subsys['project_name']
        #     project_id = each_person_subsys['project_id']
        #     subsys_id = each_person_subsys['subsys_id']
        #     subsys_name = each_person_subsys['subsys_name']
        #     total = each_person_subsys['total']
        #     # 过滤掉非测试人员
        #     if tester not in tester_standard_set:
        #         continue
        #     if tester not in each_tester_name_set:
        #         # 新的测试人员数据，则初始化项目容器
        #         project_ids_tmp = set()
        #         # 判断是否已有上个数据结果
        #         if person_data_dict:
        #             person_data_dict['person_total_Bug'] = person_total_Bug
        #             person_Bug_list.append(person_data_dict)
        #             # 清空个人数据结果
        #             person_data_dict = {}
        #             person_total_Bug = 0
        #         person_data_dict['person_name'] = tester
        #         person_data_dict['project_Bug_list'] = []
        #         # 加入到项目id池中
        #         project_ids_tmp.add(project_id)
        #         # 初始化Bug总数
        #         project_Bug_nums = 0
        #         # 初始化返回数据结构
        #         person_data_dict['project_Bug_list'].append(
        #             {
        #                 "project_id": project_id,
        #                 "project_name": project_name,
        #                 "Bug_num": project_Bug_nums,
        #                 "subsys_Bug_list": [
        #                     {
        #                         "subsys_id": subsys_id,
        #                         "subsys_name": subsys_name,
        #                         "Bug_num": total,
        #                     }
        #                 ]
        #              }
        #         )
        #         # 此测试人员的信息做更新
        #         each_tester_name_set.add(tester)
        #     # 处理项目与子系统的数据
        #     else:
        #         # 没有统计过的项目，统计项目与子系统
        #         if project_id not in project_ids_tmp:
        #             project_ids_tmp.add(project_id)
        #             # 初始化Bug总数
        #             project_Bug_nums = 0
        #             person_data_dict['project_Bug_list'].append(
        #                 {
        #                     "project_id": project_id,
        #                     "project_name": project_name,
        #                     "Bug_num": project_Bug_nums,
        #                     "subsys_Bug_list": [
        #                         {
        #                             "subsys_id": subsys_id,
        #                             "subsys_name": subsys_name,
        #                             "Bug_num": total,
        #                         }
        #                     ]
        #                  }
        #             )
        #
        #         # 已统计过的项目，统计子系统
        #         else:
        #             person_data_dict['project_Bug_list'][-1]['subsys_Bug_list'].append(
        #                 {
        #                     "subsys_id": subsys_id,
        #                     "subsys_name": subsys_name,
        #                     "Bug_num": total,
        #                 }
        #             )
        #     # 更新项目Bug数
        #     project_Bug_nums += total
        #     person_data_dict['project_Bug_list'][-1]['Bug_num'] = project_Bug_nums
        #     # 统计本人的所有Bug的数据结果更新
        #     person_total_Bug += project_Bug_nums
        # else:
        #     person_data_dict['person_total_Bug'] = person_total_Bug
        #     person_Bug_list.append(person_data_dict)
        # 组织结果数据
        # 返回数据结构构造
        json_returns = {'data': {'person_bug_list': person_Bug_Reopen_search_result},
                        'total': person_Bug_Reopen_all.get('total', 0),
                        'date_verify_time_start': date_verify_time_start,
                        'date_verify_time_end': date_verify_time_end,
                        'severity': severity
                        }
        # json_returns = {'data': {'person_bug_list': person_Bug_list}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def projectList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # subsys_id = int(request.GET.get('subsys_id', 0))

        print(f'【Bug】project_sql: {Bug_all_project_name_list_sql}')
        # 查询测试人员名单
        project_list = cursor.search_alone(Bug_all_project_name_list_sql)
        # 这里是为了查询没有关联项目的人员
        project_list.insert(0,
            {'id': 0, 'name': ' ', 'status': ' '}
        )
        # print(f'【Bug】project 列表的查询结果: {project_list}')
        json_returns = {
            'data': {
                "project_names": project_list,
                'total': len(project_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def subsysList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        project_id = int(request.GET.get('project_id', 0))
        # 对传参做判断与分类
        if project_id:
            subsys_sql = construct_sql_Bug_subsys_by_project_search_sql(
                _project_id=project_id
            )
        elif project_id <= 0 or not project_id:
            subsys_sql = construct_sql_Bug_subsys_by_project_search_sql()
        print(f'【Bug】subsys_sql: {subsys_sql}')
        # 查询测试人员名单
        subsys_list = cursor.search_alone(subsys_sql)
        # print(f'【Bug】subsys 列表的查询结果: {subsys_list}')
        json_returns = {
            'data': {
                "subsys_names": subsys_list,
                'total': len(subsys_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def assignerList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))
        # 查询测试人员名单
        assigner_name_list = cursor.search_alone(Bug_assigner_name_list_sql)
        print(assigner_name_list)

        json_returns = {
            'data': {
                "assigner_names": assigner_name_list,
                'total': len(assigner_name_list)
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


# Bug状态下拉列表
async def BugstatusList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))

        json_returns = {
            'data': {
                "Bug_status": Bug_status_constant_list,
                'total': len(Bug_status_constant_list)
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


# Bug解决方案下拉列表
async def BugResolutionList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))

        json_returns = {
            'data': {
                "Bug_status": [{'status': each} for each in resolution_name_type_corresponding_Chinese.keys()],
                'total': len(resolution_name_type)
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



