from django.contrib import admin
from django.urls import path
from personData import views

urlpatterns = [
    # 测试人员
    path('caseExecuteTotal', views.caseExecuteTotal),
    path('caseCreateTotal', views.caseCreateTotal),
    path('BugCreateTotal', views.BugCreateTotal),
    path('BugVerifyTotal', views.BugVerifyTotal),
    path('BugCloseTotal', views.BugCloseTotal),
    path('BugReopenTotal', views.BugReopenTotal),
    # 开发人员
    path('developerBugDataStatistics', views.developerBugDataStatistics),

]
