import json

from asgiref.sync import sync_to_async
from django.http import HttpResponse, HttpResponseBadRequest

# Create your views here.
from common.constant import department_person_file, pkl_key_dict, HalfDay, TenMinutes
from common.sql_constant_enum import case_type_constant_responding_English, case_type_constant_responding_English_all
from common.sql_instance_config import department_person_info_list_sql
from common.sql_instance_persondata import *
from utils.cursorHandler import cursor, CursorHandler
from utils.normal_simple_function import serialize_json_dumps, fetch_file_abs_path, read_pkl_file, redis_data_get, \
    redis_hot_data_read, get_self_function_name, redis_hot_data_save, time_parse_string


async def caseExecuteTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # case创建人
        executor = params.get('tester', None)
        # case创建起始时间
        date_execute_time_start = params.get('date_time_start', None)
        # case创建终止时间
        date_execute_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)
        case_types = case_type_constant_responding_English_all.keys()
        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))

        # 满足初始化页面加载条件
        if not any([date_execute_time_start, date_execute_time_end]):
            # 从redis中获取热数据的判决
            all_tester_case_execute_data = redis_hot_data_read(get_self_function_name())
            if all_tester_case_execute_data != []:
                # 对于查询个人的数据
                all_tester_case_execute_data = [this_person for this_person in all_tester_case_execute_data for every in executor if this_person['person_name'] == every] if executor else all_tester_case_execute_data
                json_returns = {'data': {'person_case_list': all_tester_case_execute_data,
                                         'date_time_start': date_execute_time_start,
                                         'date_time_end': date_execute_time_end}}
                return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")

        # 如果是限定日期查询，则直接查询数据库
        # 按照数据类型分类
        case_execute_statistics_sql = construct_person_case_execute_data_sql(
            _date_start_time=date_execute_time_start,
            _date_end_time=date_execute_time_end,
            _tester=tuple(executor)
        )
        print(case_execute_statistics_sql)
        person_case_execute_search_result = cursor.search_alone(case_execute_statistics_sql)
        person_case_execute_search_result_hash_map = {each_person['tester']: {
            "total": each_person['total'],
            "project_cases": [
                {
                    "type": case_type_key,
                    "nums": each_person[case_type_key]
                } for case_type_key in case_types
            ]
        } for each_person in person_case_execute_search_result}
        # print('person_case_execute_search_result', person_case_execute_search_result)
        # 结果做记录
        all_tester_case_execute_data = []
        if executor:
            for each in executor:
                if person_case_execute_search_result_hash_map.get(each):
                    all_tester_case_execute_data.append(
                        {
                            "person_name": each,
                             **person_case_execute_search_result_hash_map[each]
                        }
                    )
                else:
                    all_tester_case_execute_data.append(
                        {
                            "person_name": each,
                            "total": 0,
                            "project_cases": [
                                {
                                    "type": case_type_key,
                                    "nums": 0
                                } for case_type_key in case_types
                            ]
                        }
                    )
        if not executor:
            for person in tester_standard_set:
                if person not in person_case_execute_search_result_hash_map:
                    all_tester_case_execute_data.append(
                        {
                            "person_name": person,
                            "total": 0,
                            "project_cases": [
                                {
                                    "type": case_type_key,
                                    "nums": 0
                                } for case_type_key in case_types
                            ]
                        }
                    )
                else:
                    all_tester_case_execute_data.append(
                        {
                            "person_name": person,
                            **person_case_execute_search_result_hash_map[person],
                        }
                    )

        # 将热数据进行存储
        if not any([executor, date_execute_time_start, date_execute_time_end]):
            redis_hot_data_save(key_name=get_self_function_name(), value_data=all_tester_case_execute_data, data_timeout=TenMinutes)
        # 计算功能用例，与非功能用例的总数
        tmp_list = []
        unfeature_num = 0
        for each_index in range(len(all_tester_case_execute_data)):
            each = all_tester_case_execute_data[each_index]
            for case_num in each['project_cases']:
                if case_num['type'] == 'feature':
                    tmp_list.append({'type': 'feature', 'nums': case_num['nums']})
                else:
                    unfeature_num += case_num['nums']
            else:
                tmp_list.append({'type': 'unfeature', 'nums': unfeature_num})
                each['project_cases'] = tmp_list
                tmp_list = []
                unfeature_num = 0
        json_returns = {'data': {'person_case_list': all_tester_case_execute_data,
                                 'date_time_start': date_execute_time_start,
                                 'date_time_end': date_execute_time_end}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def caseCreateTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # case创建人
        tester = params.get('tester', None)
        # case创建起始时间
        date_time_start = params.get('date_time_start', None)
        # case创建终止时间
        date_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)
        case_types = case_type_constant_responding_English_all.keys()
        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))

        # 满足初始化页面加载条件
        if not any([date_time_start, date_time_end]):
            # 从redis中获取热数据的判决
            all_tester_case_create_data = redis_hot_data_read(get_self_function_name())
            if all_tester_case_create_data != []:
                # 对于查询个人的数据
                all_tester_case_create_data = [this_person for this_person in all_tester_case_create_data for every in tester if this_person['person_name'] == every] if tester else all_tester_case_create_data
                json_returns = {'data': {'person_case_list': all_tester_case_create_data,
                                         'date_time_start': date_time_start,
                                         'date_time_end': date_time_end}}
                return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")

        # 按照数据类型分类
        case_create_statistics_sql = construct_person_case_create_data_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _tester=tuple(tester)
        )
        print(case_create_statistics_sql)
        person_case_create_search_result = cursor.search_alone(case_create_statistics_sql)
        person_case_create_search_result_hash_map = {each_person['tester']: {
            "total": each_person['total'],
            "project_cases": [
                {
                    "type": case_type_key,
                    "nums": each_person[case_type_key]
                } for case_type_key in case_types
            ]
        } for each_person in person_case_create_search_result}
        # print('person_case_create_search_result', person_case_create_search_result)
        all_tester_case_create_data = []
        if tester:
            for each in tester:
                if person_case_create_search_result_hash_map.get(each):
                    all_tester_case_create_data.append(
                        {
                            "person_name": each,
                             **person_case_create_search_result_hash_map[each]
                        }
                    )
                else:
                    all_tester_case_create_data.append(
                        {
                            "person_name": each,
                            "total": 0,
                            "project_cases": [
                                {
                                    "type": case_type_key,
                                    "nums": 0
                                } for case_type_key in case_types
                            ]
                        }
                    )
            # 计算功能用例，与非功能用例的总数

            for each_index in range(len(all_tester_case_create_data)):
                tmp_list = []
                unfeature_num = 0
                each = all_tester_case_create_data[each_index]
                for case_num in each['project_cases']:
                    if case_num['type'] == 'feature':
                        tmp_list.append({'type': 'feature', 'nums': case_num['nums']})
                    else:
                        unfeature_num += case_num['nums']
                else:
                    tmp_list.append({'type': 'unfeature', 'nums': unfeature_num})
                    each['project_cases'] = tmp_list
            json_returns = {'data': {'person_case_list': all_tester_case_create_data,
                                     'date_time_start': date_time_start,
                                     'date_time_end': date_time_end}}
            return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        for person in tester_standard_set:
            if person not in person_case_create_search_result_hash_map:
                all_tester_case_create_data.append(
                    {
                        "person_name": person,
                        "total": 0,
                        "project_cases": [
                            {
                                "type": case_type_key,
                                "nums": 0
                            } for case_type_key in case_types
                        ]
                    }
                )
            else:
                all_tester_case_create_data.append(
                    {
                        "person_name": person,
                        **person_case_create_search_result_hash_map[person],
                    }
                )

        # 将热数据进行存储
        if not any([tester, date_time_start, date_time_end]):
            redis_hot_data_save(key_name=get_self_function_name(), value_data=all_tester_case_create_data, data_timeout=TenMinutes)

        for each_index in range(len(all_tester_case_create_data)):
            tmp_list = []
            unfeature_num = 0
            each = all_tester_case_create_data[each_index]
            for case_num in each['project_cases']:
                if case_num['type'] == 'feature':
                    tmp_list.append({'type': 'feature', 'nums': case_num['nums']})
                else:
                    unfeature_num += case_num['nums']
            else:
                tmp_list.append({'type': 'unfeature', 'nums': unfeature_num})
                each['project_cases'] = tmp_list

        json_returns = {'data': {'person_case_list': all_tester_case_create_data,
                                 'date_time_start': date_time_start,
                                 'date_time_end': date_time_end}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugCreateTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # case创建人
        tester = params.get('tester', None)
        # case创建起始时间
        date_time_start = params.get('date_time_start', None)
        # case创建终止时间
        date_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))

        # 满足初始化页面加载条件
        if not any([date_time_start, date_time_end]):
            # 从redis中获取热数据的判决
            all_tester_Bug_create_data = redis_hot_data_read(get_self_function_name())
            if all_tester_Bug_create_data != []:
                # 对于查询个人的数据
                all_tester_Bug_create_data = [this_person for this_person in all_tester_Bug_create_data for every in tester if this_person['person_name'] == every] if tester else all_tester_Bug_create_data
                json_returns = {'data': {'person_case_list': all_tester_Bug_create_data,
                                         'date_time_start': date_time_start,
                                         'date_time_end': date_time_end}}
                return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")

        # 按照数据类型分类
        Bug_create_statistics_sql = construct_person_Bug_create_data_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _tester=tuple(tester)
        )
        print(Bug_create_statistics_sql)
        person_Bug_create_search_result = cursor.search_alone(Bug_create_statistics_sql)
        person_Bug_create_search_result_hash_map = {each_person['tester']: {
            "total": each_person['total'],
        } for each_person in person_Bug_create_search_result}
        # print('person_Bug_create_search_result', person_Bug_create_search_result)
        all_tester_Bug_create_data = []
        if tester:
            for each in tester:
                if person_Bug_create_search_result_hash_map.get(each):
                    all_tester_Bug_create_data.append(
                        {
                            "person_name": each,
                             **person_Bug_create_search_result_hash_map[each]
                        }
                    )
                else:
                    all_tester_Bug_create_data.append(
                        {
                            "person_name": each,
                            "total": 0,
                        }
                    )
            json_returns = {'data': {'person_case_list': all_tester_Bug_create_data,
                                     'date_time_start': date_time_start,
                                     'date_time_end': date_time_end}}
            return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        for person in tester_standard_set:
            if person not in person_Bug_create_search_result_hash_map:
                all_tester_Bug_create_data.append(
                    {
                        "person_name": person,
                        "total": 0,
                    }
                )
            else:
                all_tester_Bug_create_data.append(
                    {
                        "person_name": person,
                        **person_Bug_create_search_result_hash_map[person],
                    }
                )
        # 将热数据进行存储
        if not any([tester, date_time_start, date_time_end]):
            redis_hot_data_save(key_name=get_self_function_name(), value_data=all_tester_Bug_create_data, data_timeout=TenMinutes)

        json_returns = {'data': {'person_case_list': all_tester_Bug_create_data,
                                 'date_time_start': date_time_start,
                                 'date_time_end': date_time_end}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugVerifyTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # case创建人
        tester = params.get('tester', None)
        # case创建起始时间
        date_time_start = params.get('date_time_start', None)
        # case创建终止时间
        date_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 这里对多个月的时间范围做判断和处理
        if date_time_end and date_time_start:
            time_mouth_list = []
            start_time_stru = time_parse_string(date_time_start, to_time_type="struct_time")
            end_time_stru = time_parse_string(date_time_end, to_time_type="struct_time")
            # 要么月大于，要么年大于
            if end_time_stru.month > start_time_stru.month or end_time_stru.year > start_time_stru.year:
                total_months = (end_time_stru.year - start_time_stru.year) * 12 + end_time_stru.month - start_time_stru.month
                time_mouth_list = [
                    f"{start_time_stru.year + (start_time_stru.month + month_index) // 12}-{(start_time_stru.month + month_index) % 12 if ((start_time_stru.month + month_index) % 12) > 0 else 12}-{start_time_stru.day} {start_time_stru.hour}:{start_time_stru.minute}:{start_time_stru.second}"
                    for month_index in range(1, total_months + 1)
                ]

                # 这里处理日期是否是同一天
                if time_parse_string(time_mouth_list[-1]) != time_parse_string(date_time_end):
                    # 如果选择的截止日期小于计算的截止日期
                    if time_parse_string(time_mouth_list[-1]) > time_parse_string(date_time_end):
                        time_mouth_list[-1] = date_time_end
                    # 选择的截止日期大于计算的截止日期，则需要将截止日期添加在内
                    else:
                        time_mouth_list.append(date_time_end)
            # 只有日期大于
            else:
                time_mouth_list.append(date_time_end)

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))

        # 满足初始化页面加载条件
        if not any([date_time_start, date_time_end]):
            # 从redis中获取热数据的判决
            all_tester_Bug_verify_data = redis_hot_data_read(get_self_function_name())
            if all_tester_Bug_verify_data != []:
                # 对于查询个人的数据
                all_tester_Bug_verify_data = [this_person for this_person in all_tester_Bug_verify_data for every in tester if this_person['person_name'] == every] if tester else all_tester_Bug_verify_data
                json_returns = {'data': {'person_case_list': all_tester_Bug_verify_data,
                                         'date_time_start': date_time_start,
                                         'date_time_end': date_time_end}}
                return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")

        # ------------------------------ 老代码 数据库正常查询很快 ----------
        Bug_verify_statistics_sql = construct_person_Bug_verify_data_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _tester=tuple(tester)
        )
        print(Bug_verify_statistics_sql)
        person_Bug_verify_search_result = cursor.search_alone(Bug_verify_statistics_sql)
        person_Bug_verify_search_result_hash_map = {each_person['tester']: {
            "total": each_person['total']
        } for each_person in person_Bug_verify_search_result}
        # ---------------------- 新代码协程查询异步等待查询结果 -------------
        # 按照数据类型分类
        # person_Bug_verify_search_result = []
        # for end_time_point in time_mouth_list:
        #     Bug_verify_statistics_sql = construct_person_Bug_verify_data_sql(
        #         _date_start_time=date_time_start,
        #         _date_end_time=end_time_point,
        #         _tester=tuple(tester)
        #     )
        #     results = await sync_to_async(cursor.search_alone, thread_sensitive=False)(Bug_verify_statistics_sql)
        #     person_Bug_verify_search_result.append(results)
        #     # 起始时间变更为当前的时间终点
        #     date_time_start = end_time_point
        #
        # person_Bug_verify_search_result_hash_map = {}
        # for each_month_person in person_Bug_verify_search_result:
        #     for each_person in each_month_person:
        #         if each_person['tester'] not in person_Bug_verify_search_result_hash_map:
        #             person_Bug_verify_search_result_hash_map[each_person['tester']] = {"total": each_person['total']}
        #         else:
        #             person_Bug_verify_search_result_hash_map[each_person['tester']]["total"] += each_person['total']

        # print('person_Bug_verify_search_result', person_Bug_verify_search_result)
        all_tester_Bug_verify_data = []
        if tester:
            for each in tester:
                if person_Bug_verify_search_result_hash_map.get(each):
                    all_tester_Bug_verify_data.append(
                        {
                            "person_name": each,
                             **person_Bug_verify_search_result_hash_map[each]
                        }
                    )
                else:
                    all_tester_Bug_verify_data.append(
                        {
                                "person_name": each,
                                "total": 0,
                        }
                    )
            json_returns = {'data': {'person_case_list': all_tester_Bug_verify_data,
                                     'date_time_start': date_time_start,
                                     'date_time_end': date_time_end}}
            return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        for person in tester_standard_set:
            if person not in person_Bug_verify_search_result_hash_map:
                all_tester_Bug_verify_data.append(
                    {
                        "person_name": person,
                        "total": 0,
                    }
                )
            else:
                all_tester_Bug_verify_data.append(
                    {
                        "person_name": person,
                        **person_Bug_verify_search_result_hash_map[person],
                    }
                )
        # 将热数据进行存储
        if not any([tester, date_time_start, date_time_end]):
            redis_hot_data_save(key_name=get_self_function_name(), value_data=all_tester_Bug_verify_data, data_timeout=TenMinutes)

        json_returns = {'data': {'person_case_list': all_tester_Bug_verify_data,
                                 'date_time_start': date_time_start,
                                 'date_time_end': date_time_end}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugCloseTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # case创建人
        tester = params.get('tester', None)
        # case创建起始时间
        date_time_start = params.get('date_time_start', None)
        # case创建终止时间
        date_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)

        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))

        # 满足初始化页面加载条件
        if not any([date_time_start, date_time_end]):
            # 从redis中获取热数据的判决
            all_tester_Bug_close_data = redis_hot_data_read(get_self_function_name())
            if all_tester_Bug_close_data != []:
                # 对于查询个人的数据
                all_tester_Bug_close_data = [this_person for this_person in all_tester_Bug_close_data for every in tester if this_person['person_name'] == every] if tester else all_tester_Bug_close_data
                json_returns = {'data': {'person_case_list': all_tester_Bug_close_data,
                                         'date_time_start': date_time_start,
                                         'date_time_end': date_time_end}}
                return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")

        # 按照数据类型分类
        Bug_close_statistics_sql = construct_person_Bug_close_data_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _tester=tuple(tester)
        )
        print(Bug_close_statistics_sql)
        person_Bug_close_search_result = cursor.search_alone(Bug_close_statistics_sql)
        person_Bug_close_search_result_hash_map = {each_person['tester']: {
            "total": each_person['total'],
        } for each_person in person_Bug_close_search_result}
        # print('person_Bug_close_search_result', person_Bug_close_search_result)
        all_tester_Bug_close_data = []
        if tester:
            for each in tester:
                if person_Bug_close_search_result_hash_map.get(each):
                    all_tester_Bug_close_data.append(
                        {
                            "person_name": each,
                            **person_Bug_close_search_result_hash_map[each]
                        }
                    )
                else:
                    all_tester_Bug_close_data.append(
                        {
                                "person_name": each,
                                "total": 0,
                        }
                    )
            json_returns = {'data': {'person_case_list': all_tester_Bug_close_data,
                                     'date_time_start': date_time_start,
                                     'date_time_end': date_time_end}}
            return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        for person in tester_standard_set:
            if person not in person_Bug_close_search_result_hash_map:
                all_tester_Bug_close_data.append(
                    {
                        "person_name": person,
                        "total": 0,
                    }
                )
            else:
                all_tester_Bug_close_data.append(
                    {
                        "person_name": person,
                        **person_Bug_close_search_result_hash_map[person],
                    }
                )
        # 将热数据进行存储
        if not any([tester, date_time_start, date_time_end]):
            redis_hot_data_save(key_name=get_self_function_name(), value_data=all_tester_Bug_close_data, data_timeout=TenMinutes)

        json_returns = {'data': {'person_case_list': all_tester_Bug_close_data,
                                 'date_time_start': date_time_start,
                                 'date_time_end': date_time_end}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugReopenTotal(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # case创建人
        tester = params.get('tester', None)
        # case创建起始时间
        date_time_start = params.get('date_time_start', None)
        # case创建终止时间
        date_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)
        case_types = case_type_constant_responding_English.keys()
        # 过滤非测试人员
        tester_standard_json = redis_data_get('department_person_file', search_sql=department_person_info_list_sql, data_timeout=HalfDay)
        tester_standard_set = frozenset((each_person['tester_name'] for each_person in tester_standard_json))

        # 满足初始化页面加载条件
        if not any([date_time_start, date_time_end]):
            # 从redis中获取热数据的判决
            all_tester_Bug_Reopen_data = redis_hot_data_read(get_self_function_name())
            if all_tester_Bug_Reopen_data != []:
                # 对于查询个人的数据
                all_tester_Bug_Reopen_data = [this_person for this_person in all_tester_Bug_Reopen_data for every in tester if this_person['person_name'] == every] if tester else all_tester_Bug_Reopen_data
                json_returns = {'data': {'person_case_list': all_tester_Bug_Reopen_data,
                                         'date_time_start': date_time_start,
                                         'date_time_end': date_time_end}}
                return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")

        # 按照数据类型分类
        Bug_Reopen_statistics_sql = construct_person_Bug_Reopen_data_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _tester=tuple(tester)
        )
        print(Bug_Reopen_statistics_sql)
        person_Bug_Reopen_search_result = cursor.search_alone(Bug_Reopen_statistics_sql)
        person_Bug_Reopen_search_result_hash_map = {each_person['tester']: {
            "total": each_person['total'],
        } for each_person in person_Bug_Reopen_search_result}
        # print('person_Bug_Reopen_search_result', person_Bug_Reopen_search_result)
        all_tester_Bug_Reopen_data = []
        if tester:
            for each in tester:
                if person_Bug_Reopen_search_result_hash_map.get(each):
                    all_tester_Bug_Reopen_data.append(
                        {
                            "person_name": each,
                            **person_Bug_Reopen_search_result_hash_map[each]
                        }
                    )
                else:
                    all_tester_Bug_Reopen_data.append(
                        {
                                "person_name": each,
                                "total": 0,
                        }
                    )
            json_returns = {'data': {'person_case_list': all_tester_Bug_Reopen_data,
                                     'date_time_start': date_time_start,
                                     'date_time_end': date_time_end}}
            return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        for person in tester_standard_set:
            if person not in person_Bug_Reopen_search_result_hash_map:
                all_tester_Bug_Reopen_data.append(
                    {
                        "person_name": person,
                        "total": 0,
                    }
                )
            else:
                all_tester_Bug_Reopen_data.append(
                    {
                        "person_name": person,
                        **person_Bug_Reopen_search_result_hash_map[person],
                    }
                )
        # 将热数据进行存储
        if not any([tester, date_time_start, date_time_end]):
            redis_hot_data_save(key_name=get_self_function_name(), value_data=all_tester_Bug_Reopen_data, data_timeout=TenMinutes)

        json_returns = {'data': {'person_case_list': all_tester_Bug_Reopen_data,
                                 'date_time_start': date_time_start,
                                 'date_time_end': date_time_end}}
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def developerBugDataStatistics(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        print('-----------', params)
        # 开发人
        developer = params.get('developer', None)
        # 查询起始时间
        date_time_start = params.get('date_time_start', None)
        # 查询终止时间
        date_time_end = params.get('date_time_end', None)
        # 页码
        pageNum = params.get('pageNum', 0)
        # 页显示数
        pageSize = params.get('pageSize', 25)


        # 按照数据类型分类
        developer_data_statistics_sql = construct_developer_bug_data_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _developer=developer,
            _pageNum=pageNum,
            _pageSize=pageSize
        )
        print('developer_data_statistics_sql=>',developer_data_statistics_sql)
        developer_data_statistics_search_result = cursor.search_alone(developer_data_statistics_sql)

        all_developer_count = cursor.search_alone(construct_developer_all_sql(
            _date_start_time=date_time_start,
            _date_end_time=date_time_end,
            _developer=developer
        ))

        json_returns = {
            'data': {
                'developer_bug_list': [
                    {
                        # 开发人员名称
                        "developer": each_developer['developer'],
                        # 已解决Bug数
                        "has_solved_num": each_developer['has_solved_num'],
                        # 待确认Bug数
                        "no_confirm_num": each_developer['no_confirm_num'],
                        # 待解决Bug数
                        "no_solved_num": each_developer['no_solved_num'],
                        # 激活总数
                        "activate_count_num": each_developer['activate_count_num'],
                        # 平均激活次数
                        "average_activate_num": each_developer['average_activate_num'],
                        # 激活1次个数
                        "activate_num_1": each_developer['activate_num_1'],
                        # 激活2次个数
                        "activate_num_2": each_developer['activate_num_2'],
                        # 激活3次个数
                        "activate_num_3": each_developer['activate_num_3'],
                        # 激活3次以上个数
                        "activate_num_3_plus": each_developer['activate_num_3_plus'],
                        # Bug确认时效
                        "confirm_hours": each_developer['confirm_hours'],
                        # Bug解决时效
                        "solved_hours": each_developer['solved_hours']
                    } for each_developer in developer_data_statistics_search_result
                ],
                'total':  all_developer_count[0].get('developer_num')
            }
        }
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


