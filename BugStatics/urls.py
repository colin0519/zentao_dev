from django.contrib import admin
from django.urls import path
from BugStatics import views

urlpatterns = [
    path('BugStatistics', views.BugStatistics),
    path('testerNameList', views.testerNameList),
    path('verisonNOList', views.verisonNOList),
    path('subsysList', views.subsysList),
    path('projectList', views.projectList),
    path('creatorList', views.creatorList),
    path('assignerList', views.assignerList),
    path('BugstatusList', views.BugstatusList),
    path('BugResolutionList', views.BugResolutionList),
    path('BugCreateShow', views.BugCreateShow),
    path('BugVerifyShow', views.BugVerifyShow),
    path('BugCloseShow', views.BugCloseShow),
    path('BugCreateShowByDate', views.BugCreateShowByDate),
    path('BugVerifyShowByDate', views.BugVerifyShowByDate),
    path('BugCloseShowByDate', views.BugCloseShowByDate),
    path('BugCreateTimeSlotShow', views.BugCreateTimeSlotShow),
    path('BugCloseTimeSlotShow', views.BugCloseTimeSlotShow),
]

