import json

# Create your views here.
from pprint import pprint

from django.http import HttpResponse, HttpResponseBadRequest

from common.sql_instance_project import Bug_efficient_rank_6
from utils.normal_simple_function import serialize_json_dumps, serialize_json_date_type, compute_rank_6_efficient
from common.sql_instance_bug import *
from common.sql_constant_enum import *
from utils.cursorHandler import cursor
import asyncio

def BugStatistics_old(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        tag_type = params.get('tag_type', None)
        Bug_type = params.get('Bug_type', None)
        Bug_status = params.get('Bug_status', None)
        subsystem = params.get('subsystem', None)
        milestone_verison = params.get('milestone_verison', None)
        verison_NO = params.get('verison_NO', None)
        tester_name = params.get('tester_name', None)
        date_range = params.get('date_range', None)
        resolution = params.get('resolution', None)
        # 数据类型
        data_type = params.get('data_type', None)
        # Bug 条件查询的数据结果
        Bug_condition_search_result = []
        # TODO  1. Bug有效率 与 Bug总数
        # 按照数据类型分类
        # data_type: 0 "Bug有效率"
        if data_type == 0:
            # 榜单人员查询结果
            name_nums: list(dict) = cursor.search_alone(Bug_efficient_rank_6)
            # 计算有效率排行人员所有名单(SQL语句给出的按照提单数的欠30名人员)
            rank_all_efficient: list = [{each_person[0]: each_person[1][1]}
                for each_person in compute_rank_6_efficient(name_nums)
            ]

        # data_type: 1 "Bug总数"
        elif data_type == 1:
            # 榜单人员查询结果
            name_nums: list(dict) = cursor.search_alone(Bug_efficient_rank_6)
            rank_all_efficient: list = [{each_person[0]: each_person[1][2]}
                                        for each_person in compute_rank_6_efficient(name_nums)
                                        ]

        # data_type: 3 "Bug解决方案", data_type: 2 "Bug状态"
        elif data_type in (2, 3):
            print(construct_sql_Bug_statistics_detail(tag_type, Bug_status, subsystem, milestone_verison, verison_NO, tester_name, date_range, resolution_name_type.get(resolution, None)))

        Bug_condition_search_result = cursor.search_alone(construct_sql_Bug_statistics_detail(tag_type, Bug_status, subsystem, milestone_verison, verison_NO, tester_name, date_range, resolution_name_type.get(resolution, None)))
        # 返回数据结构的构造
        json_returns = {
            'data': {
                "Bug_total": len(Bug_condition_search_result),  # Bug总数
                "tag_type": tag_type,  # 标签类型
                "Bug_type": Bug_type,  # 数据类型
                "subsystem": subsystem,  # 子系统条件
                "milestone_verison": milestone_verison,  # 版本类型
                "verison_NO": verison_NO,  # 版本号
                "tester_name": tester_name,  # 姓名
                "date_range": date_range,  # 时间
                "all_efficent_data": locals().get('rank_all_efficient', []), # 有效率排名人员名单
                # Bug详情清单
                "Bug_detail_list": [{
                    # BugId
                    "Bug_id": each_bug.get('bug_id'),
                    # Bug标题
                    "Bug_title": each_bug.get('title'),
                    # Bug状态
                    "Bug_status": each_bug.get('status'),
                    # Bug优先级
                    "Bug_priority": each_bug.get('priority'),
                    # 创建人
                    "creator": each_bug.get('creator'),
                    # 创建时间
                    "create_date": serialize_json_date_type(each_bug.get('create_date')),
                    # 解决方案
                    "solve_situation": each_bug.get('resolution')
                } for each_bug in Bug_condition_search_result if each_bug]
            }
        }
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugStatistics(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 项目id
        project_id = params.get('project_id', None)
        # 子系统id
        subsys_id = params.get('subsys_id', None)
        # Bug创建人
        creator_name = params.get('creator_name', None)
        # 指派人
        assigner_name = params.get('assigner_name', None)
        # 创建起始日期
        date_create_start = params.get('date_create_start', None)
        # 创建截止日期
        date_create_end = params.get('date_create_end', None)
        # 解决的起始日期
        date_solve_start = params.get('date_solve_start', None)
        # 解决的截止日期
        date_solve_end = params.get('date_solve_end', None)
        # 解决方案
        solve_resolution = params.get('solve_resolution', None)
        # Bug状态
        Bug_status = params.get('Bug_status', None)
        # 第 n 页
        pageNo = params.get('pageNo', 0)
        # 显示数量
        pageNum = params.get('pageNum', 25)
        # Bug 条件查询的数据结果
        Bug_condition_search_result = []
        Bug_condition_search_sql = construct_sql_Bug_statistics_detail(
                _project_id=project_id,
                _subsys_id=subsys_id,
                _creator_name=creator_name,
                _assigner_name=assigner_name,
                _date_create_start=date_create_start,
                _date_create_end=date_create_end,
                _date_solve_start=date_solve_start,
                _date_solve_end=date_solve_end,
                _solve_resolution=resolution_name_type_corresponding_Chinese.get(solve_resolution),
                _Bug_status=Bug_status_constant_responding_Chinese.get(Bug_status),
                _pageNo=pageNo,
                _pageNum=pageNum
        )
        print(Bug_condition_search_sql)
        Bug_condition_search_all_sql = construct_sql_Bug_statistics_all(
            _project_id=project_id,
            _subsys_id=subsys_id,
            _creator_name=creator_name,
            _assigner_name=assigner_name,
            _date_create_start=date_create_start,
            _date_create_end=date_create_end,
            _date_solve_start=date_solve_start,
            _date_solve_end=date_solve_end,
            _solve_resolution=resolution_name_type_corresponding_Chinese.get(solve_resolution),
            _Bug_status=Bug_status_constant_responding_Chinese.get(Bug_status)
        )
        print('Bug_condition_search_all_sql', Bug_condition_search_all_sql)
        Bug_condition_search_result = cursor.search_alone(Bug_condition_search_sql)
        Bug_condition_search_all_result = cursor.search_alone(Bug_condition_search_all_sql)[0].get('total')
        json_returns = {
            'data': {
                "Bug_total": Bug_condition_search_all_result,  # Bug总数
                "project_id": project_id, # 项目id
                "subsys_id": subsys_id, # 子系统id
                "creator_name": creator_name, # Bug创建人
                "assigner_name": assigner_name, # 指派人
                "date_create_start": date_create_start, # 创建起始日期
                "date_create_end": date_create_end, # 创建截止日期
                "solve_resolution": solve_resolution, # 解决方案
                "date_solve_start": date_solve_start, # 解决的起始日期
                "date_solve_end": date_solve_end, # 解决的截止日期
                "Bug_status": Bug_status, # Bug状态
                "Bug_detail_list": [{
                    # BugId
                    "Bug_id": each_bug.get('bug_id'),
                    # 项目id
                    "project_id": each_bug.get('project_id'),
                    # 子系统id
                    "subsys_id": each_bug.get('subsys_id'),
                    # 项目名称
                    "project_name": each_bug.get('project_name'),
                    # 子系统名称
                    "subsys_name": each_bug.get('subsys_name'),
                    # Bug严重程度
                    "Bug_priority": each_bug.get('severity'),
                    # Bug标题
                    "Bug_title": each_bug.get('title'),
                    # 创建人
                    "creator": each_bug.get('creator'),
                    # 创建时间
                    "create_date": serialize_json_date_type(each_bug.get('create_date')),
                    # 创建人
                    "assigner": each_bug.get('assigner'),
                    # 创建时间
                    "sovled_date": serialize_json_date_type(each_bug.get('sovled_date')),
                    # Bug状态
                    "Bug_status": Bug_status_constant_responding_English.get(each_bug.get('status')),
                    # 解决方案
                    "solve_situation": resolution_name_type_corresponding_English.get(each_bug.get('resolution'))
                } for each_bug in Bug_condition_search_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def testerNameList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))
        # 查询测试人员名单

        tester_name_list = cursor.search_alone(Bug_tester_name_list_sql)
        print(tester_name_list)
        json_returns = {
            'data': {
                "tester_names": tester_name_list,
                'total': len(tester_name_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def verisonNOList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        subsys_id = int(request.GET.get('subsys_id', 0))
        # 对传参做判断与分类
        if subsys_id:
            version_sql = construct_sql_Bug_version_by_subsys_search_sql(
                _subsys_id=subsys_id
            )
        elif subsys_id == 0 or not subsys_id:
            version_sql = construct_sql_Bug_version_by_subsys_search_sql()

        print(f'【Bug】version_sql: {version_sql}')
        # 查询测试人员名单
        version_name_list = cursor.search_alone(version_sql)
        print(f'【Bug】version_NO查询结果: {version_name_list}')
        json_returns = {
            'data': {
                "version_names": version_name_list,
                'total': len(version_name_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def subsysList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        project_id = int(request.GET.get('project_id', 0))
        # 对传参做判断与分类
        if project_id:
            subsys_sql = construct_sql_Bug_subsys_by_project_search_sql(
                _project_id=project_id
            )
        elif project_id <= 0 or not project_id:
            subsys_sql = construct_sql_Bug_subsys_by_project_search_sql()
        print(f'【Bug】subsys_sql: {subsys_sql}')
        # 查询测试人员名单
        subsys_list = cursor.search_alone(subsys_sql)
        # print(f'【Bug】subsys 列表的查询结果: {subsys_list}')
        json_returns = {
            'data': {
                "subsys_names": subsys_list,
                'total': len(subsys_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def projectList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # subsys_id = int(request.GET.get('subsys_id', 0))

        print(f'【Bug】project_sql: {Bug_all_project_name_list_sql}')
        # 查询测试人员名单
        project_list = cursor.search_alone(Bug_all_project_name_list_sql)
        # 这里是为了查询没有关联项目的人员
        project_list.insert(0,
            {'id': 0, 'name': ' ', 'status': ' '}
        )
        # print(f'【Bug】project 列表的查询结果: {project_list}')
        json_returns = {
            'data': {
                "project_names": project_list,
                'total': len(project_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def creatorList(request):
    if request.method == 'GET':
        creator_name_list = cursor.search_alone(Bug_creator_name_list_sql)
        print(creator_name_list)
        json_returns = {
            'data': {
                "creator_names": creator_name_list,
                'total': len(creator_name_list)
            }
        }
        response = HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def assignerList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))
        # 查询测试人员名单
        assigner_name_list = cursor.search_alone(Bug_assigner_name_list_sql)
        print(assigner_name_list)

        json_returns = {
            'data': {
                "assigner_names": assigner_name_list,
                'total': len(assigner_name_list)
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


# Bug状态下拉列表
async def BugstatusList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))

        json_returns = {
            'data': {
                "Bug_status": Bug_status_constant_list,
                'total': len(Bug_status_constant_list)
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



# Bug解决方案下拉列表
async def BugResolutionList(request):
    if request.method == 'GET':
        # 获取get请求带的参数
        # date_range = int(request.GET.get('date_range', 7))

        json_returns = {
            'data': {
                "Bug_status": [{'status': each} for each in resolution_name_type_corresponding_Chinese.keys()],
                'total': len(resolution_name_type)
            }
        }
        response =  HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
        return response
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def BugCreateShow(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # Bug创建起始时间
        date_bug_create_start = params.get('date_bug_create_start', None)
        # Bug创建行截止日期
        date_bug_create_end = params.get('date_bug_create_end', None)

        # Bug_create_search_sql = construct_sql_Bug_create_all_with_detail(
        #     _date_bug_create_start=date_bug_create_start,
        #     _date_bug_create_end=date_bug_create_end
        # )
        Bug_create_search_sql_all = construct_sql_Bug_create_all_with_detail_by_all(
            _date_bug_create_start=date_bug_create_start,
            _date_bug_create_end=date_bug_create_end
        )
        Bug_create_search_sql_each = construct_sql_Bug_create_all_with_detail_by_each(
            _date_bug_create_start=date_bug_create_start,
            _date_bug_create_end=date_bug_create_end
        )

        print(Bug_create_search_sql_each)
        # Bug_create_search_result = cursor.search_alone(Bug_create_search_sql)
        Bug_create_search_result_all = cursor.search_alone(Bug_create_search_sql_all)
        Bug_create_search_result_each = cursor.search_alone(Bug_create_search_sql_each)
        all_Bug_create_output_result = list(Bug_create_search_result_all) + list(Bug_create_search_result_each)
        # print(Bug_create_search_result[:3])

        json_returns = {
            'data': {
                "date_bug_create_start": date_bug_create_start, # Bug创建起始时间
                "date_bug_create_end": date_bug_create_end, # Bug创建行截止日期
                "Bug_detail_list": [{
                    # Bug创建人
                    "creator": each_bug.get('creator'),
                    # 工作日
                    "weekday": weekday_name_corresponding_Chinese.get(each_bug.get('week_day'),  "【总数】"),
                    # 周一
                    "Monday": each_bug.get('Monday'),
                    # 周二
                    "Tuesday": each_bug.get('Tuesday'),
                    # 周三
                    "Wednesday": each_bug.get('Wednesday'),
                    # 周四
                    "Thursday": each_bug.get('Thursday'),
                    # 周五
                    "Friday": each_bug.get('Friday'),
                    # 周六
                    "Saturday": each_bug.get('Saturday'),
                    # 周日
                    "Sunday": each_bug.get('Sunday'),
                    # Bug创建总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_create_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugVerifyShow(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # Bug创建起始时间
        date_bug_verify_start = params.get('date_bug_verify_start', None)
        # Bug创建行截止日期
        date_bug_verify_end = params.get('date_bug_verify_end', None)

        # Bug_verify_search_sql = construct_sql_Bug_verify_all_with_detail(
        #     _date_bug_verify_start=date_bug_verify_start,
        #     _date_bug_verify_end=date_bug_verify_end
        # )
        Bug_verify_search_sql_all = construct_sql_Bug_verify_all_with_all(
            _date_bug_verify_start=date_bug_verify_start,
            _date_bug_verify_end=date_bug_verify_end
        )
        Bug_verify_search_sql_each = construct_sql_Bug_verify_all_with_each(
            _date_bug_verify_start=date_bug_verify_start,
            _date_bug_verify_end=date_bug_verify_end
        )
        print(Bug_verify_search_sql_each)
        # Bug_verify_search_result = cursor.search_alone(Bug_verify_search_sql)
        Bug_verify_search_result_all = cursor.search_alone(Bug_verify_search_sql_all)
        Bug_verify_search_result_each = cursor.search_alone(Bug_verify_search_sql_each)
        all_Bug_verify_output_result = list(Bug_verify_search_result_all) + list(Bug_verify_search_result_each)

        json_returns = {
            'data': {
                "date_bug_verify_start": date_bug_verify_start, # Bug创建起始时间
                "date_bug_verify_end": date_bug_verify_end, # Bug创建行截止日期
                "Bug_detail_list": [{
                    # Bug创建人
                    "actor": each_bug.get('actor'),
                    # 工作日
                    "weekday": weekday_name_corresponding_Chinese.get(each_bug.get('week_day'), "【总数】"),
                    # 周一
                    "Monday": each_bug.get('Monday'),
                    # 周二
                    "Tuesday": each_bug.get('Tuesday'),
                    # 周三
                    "Wednesday": each_bug.get('Wednesday'),
                    # 周四
                    "Thursday": each_bug.get('Thursday'),
                    # 周五
                    "Friday": each_bug.get('Friday'),
                    # 周六
                    "Saturday": each_bug.get('Saturday'),
                    # 周日
                    "Sunday": each_bug.get('Sunday'),
                    # Bug验证总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_verify_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def BugCloseShow(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # Bug创建起始时间
        date_bug_close_start = params.get('date_bug_close_start', None)
        # Bug创建行截止日期
        date_bug_close_end = params.get('date_bug_close_end', None)

        Bug_close_search_sql = construct_sql_Bug_close_all_with_detail(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end
        )
        Bug_close_search_sql_all = construct_sql_Bug_close_all_with_all(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end
        )
        Bug_close_search_sql_each = construct_sql_Bug_close_all_with_each(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end
        )

        print(Bug_close_search_sql_each)
        # Bug_close_search_result = cursor.search_alone(Bug_close_search_sql)
        Bug_close_search_result_all = cursor.search_alone(Bug_close_search_sql_all)
        Bug_close_search_result_each = cursor.search_alone(Bug_close_search_sql_each)
        all_Bug_close_output_result = list(Bug_close_search_result_all) + list(Bug_close_search_result_each)
        print("Bug_close_search_result", all_Bug_close_output_result[:2])

        json_returns = {
            'data': {
                "date_bug_close_start": date_bug_close_start, # Bug创建起始时间
                "date_bug_close_end": date_bug_close_end, # Bug创建行截止日期
                "Bug_detail_list": [{
                    # Bug创建人
                    "closer": each_bug.get('closer'),
                    # 工作日
                    "weekday": weekday_name_corresponding_Chinese.get(str(each_bug.get('week_day'))) if each_bug.get('week_day') is not None and '总数' not in str(each_bug.get('week_day')) else each_bug.get('week_day'),
                    # 周一
                    "Monday": each_bug.get('Monday'),
                    # 周二
                    "Tuesday": each_bug.get('Tuesday'),
                    # 周三
                    "Wednesday": each_bug.get('Wednesday'),
                    # 周四
                    "Thursday": each_bug.get('Thursday'),
                    # 周五
                    "Friday": each_bug.get('Friday'),
                    # 周六
                    "Saturday": each_bug.get('Saturday'),
                    # 周日
                    "Sunday": each_bug.get('Sunday'),
                    # Bug关闭总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_close_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def BugCreateShowByDate(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # Bug创建起始时间
        date_bug_create_start = params.get('date_bug_create_start', None)
        # Bug创建行截止日期
        date_bug_create_end = params.get('date_bug_create_end', None)
        # Bug创建人
        creator_name = params.get('creator_name', None)

        # Bug_create_search_sql = construct_sql_Bug_create_all_with_detail_by_date(
        #     _date_bug_create_start=date_bug_create_start,
        #     _date_bug_create_end=date_bug_create_end
        # )
        Bug_create_search_sql_all = construct_sql_Bug_create_all_with_detail_by_date_with_all(
            _date_bug_create_start=date_bug_create_start,
            _date_bug_create_end=date_bug_create_end,
            _creator_name=creator_name
        )
        Bug_create_search_sql_each = construct_sql_Bug_create_all_with_detail_by_date_with_each(
            _date_bug_create_start=date_bug_create_start,
            _date_bug_create_end=date_bug_create_end,
            _creator_name=creator_name
        )
        print(Bug_create_search_sql_each)
        # Bug_create_search_result = cursor.search_alone(Bug_create_search_sql)
        Bug_create_search_result_all = cursor.search_alone(Bug_create_search_sql_all)
        Bug_create_search_result_each = cursor.search_alone(Bug_create_search_sql_each)
        all_Bug_create_output_result = list(Bug_create_search_result_all) + list(Bug_create_search_result_each)
        print(all_Bug_create_output_result[:2])

        json_returns = {
            'data': {
                "date_bug_create_start": date_bug_create_start, # Bug创建起始时间
                "date_bug_create_end": date_bug_create_end, # Bug创建行截止日期
                "Bug_detail_list": [{
                    # Bug创建人
                    "creator": each_bug.get('creator') if each_bug.get('creator') is not None else "-不详-",
                    # 数据分布列表
                    "Bug_create_data_by_date": [
                        {"date": str(date),
                         "num": each_bug.get(str(date))
                         } for date in range(1, 32)
                    ],
                    # Bug创建总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_create_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugVerifyShowByDate(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # Bug创建起始时间
        date_bug_verify_start = params.get('date_bug_verify_start', None)
        # Bug创建行截止日期
        date_bug_verify_end = params.get('date_bug_verify_end', None)

        # Bug_verify_search_sql = construct_sql_Bug_verify_all_with_detail_by_date(
        #     _date_bug_verify_start=date_bug_verify_start,
        #     _date_bug_verify_end=date_bug_verify_end
        # )
        Bug_verify_search_sql_all = construct_sql_Bug_verify_all_with_detail_by_date_with_all(
            _date_bug_verify_start=date_bug_verify_start,
            _date_bug_verify_end=date_bug_verify_end
        )
        Bug_verify_search_sql_each = construct_sql_Bug_verify_all_with_detail_by_date_with_each(
            _date_bug_verify_start=date_bug_verify_start,
            _date_bug_verify_end=date_bug_verify_end
        )
        print(Bug_verify_search_sql_each)
        # Bug_verify_search_result = cursor.search_alone(Bug_verify_search_sql)
        Bug_verify_search_result_all = cursor.search_alone(Bug_verify_search_sql_all)
        Bug_verify_search_result_each = cursor.search_alone(Bug_verify_search_sql_each)
        all_Bug_verify_output_result = list(Bug_verify_search_result_all) + list(Bug_verify_search_result_each)

        json_returns = {
            'data': {
                "date_bug_verify_start": date_bug_verify_start, # Bug创建起始时间
                "date_bug_verify_end": date_bug_verify_end, # Bug创建行截止日期
                "Bug_detail_list": [{
                    # Bug创建人
                    "actor": each_bug.get('actor') if each_bug.get('actor') is not None else "-不详-",
                    # 数据分布列表
                    "Bug_verify_data_by_date": [
                        {"date": str(date),
                         "num": each_bug.get(str(date))
                         } for date in range(1, 32)
                    ],
                    # Bug验证总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_verify_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def BugCloseShowByDate(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # Bug创建起始时间
        date_bug_close_start = params.get('date_bug_close_start', None)
        # Bug创建行截止日期
        date_bug_close_end = params.get('date_bug_close_end', None)
        # Bug关闭人
        closer_name = params.get('closer_name', None)

        # Bug_close_search_sql = construct_sql_Bug_close_all_with_detail_by_date(
        #     _date_bug_close_start=date_bug_close_start,
        #     _date_bug_close_end=date_bug_close_end
        # )
        Bug_close_search_sql_all = construct_sql_Bug_close_all_with_detail_by_date_with_all(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end,
            _closer_name=closer_name
        )
        Bug_close_search_sql_each = construct_sql_Bug_close_all_with_detail_by_date_with_each(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end,
            _closer_name=closer_name
        )
        print(Bug_close_search_sql_each)
        # Bug_close_search_result = cursor.search_alone(Bug_close_search_sql)
        Bug_close_search_result_all = cursor.search_alone(Bug_close_search_sql_all)
        Bug_close_search_result_each = cursor.search_alone(Bug_close_search_sql_each)
        all_Bug_close_output_result = list(Bug_close_search_result_all) + list(Bug_close_search_result_each)
        print("Bug_close_search_result", all_Bug_close_output_result)

        json_returns = {
            'data': {
                "date_bug_close_start": date_bug_close_start, # Bug创建起始时间
                "date_bug_close_end": date_bug_close_end, # Bug创建行截止日期
                "Bug_detail_list": [{
                    # Bug创建人
                    "closer": each_bug.get('closer') if each_bug.get('closer') is not None else "-不详-",
                    # 数据分布列表
                    "Bug_close_data_by_date": [
                        {"date": str(date),
                         "num": each_bug.get(str(date))
                         } for date in range(1, 32)
                    ],
                    # Bug关闭总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_close_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')


async def BugCreateTimeSlotShow(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 执行的创建起始时间
        date_bug_create_start = params.get('date_bug_create_start', None)
        # 执行的创建行截止日期
        date_bug_create_end = params.get('date_bug_create_end', None)
        # Bug创建人
        creator_name = params.get('creator_name', None)

        # Bug_create_search_sql = construct_sql_Bug_create_data_distribute_by_time_slot(
        #     _date_bug_create_start=date_bug_create_start,
        #     _date_bug_create_end=date_bug_create_end
        # )
        Bug_create_search_sql_all = construct_sql_Bug_create_data_distribute_by_time_slot_with_all(
            _date_bug_create_start=date_bug_create_start,
            _date_bug_create_end=date_bug_create_end,
            _creator_name=creator_name
        )
        Bug_create_search_sql_each = construct_sql_Bug_create_data_distribute_by_time_slot_with_each(
            _date_bug_create_start=date_bug_create_start,
            _date_bug_create_end=date_bug_create_end,
            _creator_name=creator_name
        )
        print(Bug_create_search_sql_each)
        # Bug_create_search_result = cursor.search_alone(Bug_create_search_sql)
        Bug_create_search_result_all = cursor.search_alone(Bug_create_search_sql_all)
        Bug_create_search_result_each = cursor.search_alone(Bug_create_search_sql_each)
        all_Bug_create_output_result = list(Bug_create_search_result_all) + list(Bug_create_search_result_each)

        json_returns = {
            'data': {
                "date_bug_create_start": date_bug_create_start,  # Bug创建起始时间
                "date_bug_create_end": date_bug_create_end,  # Bug创建行截止日期
                "Bug_detail_list": [{
                    # 执行人
                    "creator": each_bug.get('creator'),
                    # 数据分布列表
                    "data_distribute": [
                        {
                            # 时间段
                            "timeSlot": f"{time_slot}:{'0' if time_slot < 13 else '3'}0 ~ {time_slot + 1 if time_slot < 22 else 8}:{'0' if time_slot + 1 < 13 or time_slot == 22 else '3'}0",
                            # 对应的执行数量
                            "num": each_bug.get("+" + str(time_slot))} for time_slot in range(8, 23)
                    ],
                    # 执行总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_create_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')



async def BugCloseTimeSlotShow(request):
    if request.method == 'POST':
        params_body: json = request.body
        params: dict = json.loads(params_body.decode())
        # print('-----------', params)
        # 执行的创建起始时间
        date_bug_close_start = params.get('date_bug_close_start', None)
        # 执行的创建行截止日期
        date_bug_close_end = params.get('date_bug_close_end', None)
        # Bug关闭人
        closer_name = params.get('closer_name', None)

        # Bug_close_search_sql = construct_sql_Bug_close_data_distribute_by_time_slot(
        #     _date_bug_close_start=date_bug_close_start,
        #     _date_bug_close_end=date_bug_close_end
        # )
        Bug_close_search_sql_all = construct_sql_Bug_close_data_distribute_by_time_slot_with_all(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end,
            _closer_name=closer_name
        )
        Bug_close_search_sql_each = construct_sql_Bug_close_data_distribute_by_time_slot_with_each(
            _date_bug_close_start=date_bug_close_start,
            _date_bug_close_end=date_bug_close_end,
            _closer_name=closer_name
        )
        print(Bug_close_search_sql_each)
        # bug_close_search_result = cursor.search_alone(case_execute_search_sql)
        Bug_close_search_result_all = cursor.search_alone(Bug_close_search_sql_all)
        Bug_close_search_result_each = cursor.search_alone(Bug_close_search_sql_each)
        all_Bug_close_output_result = list(Bug_close_search_result_all) + list(Bug_close_search_result_each)

        json_returns = {
            'data': {
                "date_bug_close_start": date_bug_close_start,  # Bug创建起始时间
                "date_bug_close_end": date_bug_close_end,  # Bug创建行截止日期
                "Bug_detail_list": [{
                    # 执行人
                    "closer": each_bug.get('closer'),
                    # 数据分布列表
                    "data_distribute": [
                        {
                            # 时间段
                            "timeSlot": f"{time_slot}:{'0' if time_slot < 13 else '3'}0 ~ {time_slot + 1 if time_slot < 22 else 8}:{'0' if time_slot + 1 < 13 or time_slot == 22 else '3'}0",
                            # 对应的执行数量
                            "num": each_bug.get("+" + str(time_slot))} for time_slot in range(8, 23)
                    ],
                    # 执行总数
                    "total": each_bug.get('total')
                } for each_bug in all_Bug_close_output_result if each_bug]
            }
        }
        # 返回数据结构的构造
        return HttpResponse(serialize_json_dumps(json_returns), content_type="application/json")
    else:
        return HttpResponseBadRequest(content=b'request is a bad request')




